﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Interpreter
{
    public class LINK
    {
        public TABLE PK_Table { get; set; }
        public FIELD PK_Field { get; set; }

        public TABLE FK_Table { get; set; }
        public FIELD FK_Field { get; set; }

        public LINK(TABLE pkT, FIELD pkF, TABLE fkT, FIELD fkF)
        {
            PK_Table = pkT;
            PK_Field = pkF;

            FK_Table = fkT;
            FK_Field = fkF;
        }

        public override string ToString()
        {
            return "[LINK: " + PK_Table.Name + " ! " + PK_Field.Name +
             "    <--->    " + FK_Table.Name + " ! " + FK_Field.Name + "]";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SSQL_Scaner
{
    public class Program
    {
        public static void Main(string[] args)
        {
            #region Лексический анализ
            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("SCANER ");
            ////Console.WriteLine("================\n\n");
            SCANER scaner=null;
            try
            {
                scaner = new SCANER();
                //scaner.toCheckLexicalErrors();
                scaner.toMakeAnalysis();
                scaner.toDefineLexemesType();
                //scaner.toPrintTokens();
                //scaner.toMakeLabels();
            }
            catch (FileNotFoundException exc)
            {
                Console.WriteLine("File not found!");
                Console.ReadKey();
                throw exc;
            }
            catch (TranslatorException exc)
            {
                Console.WriteLine("There is an exception : " + exc.Message);
                Console.ReadKey();
                throw exc;
            }

            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("SCANER HAS FINISHED!  Press any key... ");
            Console.WriteLine("Лексический анализ завершен успешно...\n\n");
            ////Console.WriteLine("================\n\n");
            //Console.ReadKey();
            //Console.Clear();

            #endregion

            #region Синтаксический анализ

            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("PARSING ");
            ////Console.WriteLine("================\n\n");
            PARSER parser = null;
            try
            {
                parser = new PARSER(scaner);
                parser.toMakeSyntaxAnalysis();
                            //parser.toPrintPostfixForm();
                            //parser.toPrintLabels();

                parser.toWriteIntoFile();
            }
            catch (TranslatorException exc)
            {
                if (exc.Message == "EOP")
                {
                    Console.WriteLine("ERROR: программа не соответствует грамматике");
                }
                else
                {
                    Console.WriteLine("ERROR: " + exc.Message);
                }

                //Console.WriteLine(exc.StackTrace);
                Console.ReadKey();
                throw exc;
            }
            catch (InvalidOperationException exc)
            {
                Console.WriteLine("ERROR: программа не соответствует грамматике!");
                //Console.WriteLine(exc.StackTrace);
                Console.ReadKey();
                throw exc;
            }

            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("PARSER HAS FINISHED!  Press any key... ");
            Console.WriteLine("Синтаксический анализ завершен успешно...\n\n");
            ////Console.WriteLine("================\n\n");
            //Console.ReadKey();
            #endregion

        }
    }
}
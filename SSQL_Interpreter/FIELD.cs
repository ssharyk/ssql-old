﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Interpreter
{
    public class FIELD
    {
        public int Number { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public bool IsPK { get; set; }

        public double MaxValue { get; set; }
        public double MinValue { get; set; }
        public double AvgValue { get; set; }

        public FIELD(int n, string dt, string nm, bool isPK)
        {
            Number = n;
            Type = dt;
            Name = nm;
            IsPK = isPK;

            toClearValues();
        }

        public void toClearValues()
        {
            MaxValue = Double.MinValue;
            MinValue = Double.MaxValue;
            AvgValue = 0;
        }

        public override string ToString()
        {
            return Number + " " + Name + " --> " + MinValue;
        }
    }
}

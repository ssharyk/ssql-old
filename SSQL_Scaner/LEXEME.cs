﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Scaner
{
    public class LEXEME
    {
        public int Number {get; set;}
        public string Value { get; set; }
        public LexemeType Type { get; set; }
        public int Line { get; set; }
        public int Symbol { get; set; }
        public int Ref { get; set; }
        public int EndLabel { get; set; }

        public LEXEME(int num, string val, int l, int symb)
        {
            Number = num;
            Value = val;
            Type = LexemeType.__;
            Line = l;
            Symbol = symb;
            Ref = -1;
        }

        public LEXEME(int num, string val, LexemeType t, int l, int symb)
        {
            Number = num;
            Value = val;
            Type = t;
            Line = l;
            Symbol = symb;
            Ref = -1;
        }

        public override string ToString()
        {
            return "[" + Value + "  -->  " + Type + "]";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SSQL_Scaner
{
    public class SCANER
    {
        #region Поля и свойства
        protected string _text;
        public List<LEXEME> Tokens { get; protected set; }
        public List<IDENTIFIER> Identifiers { get; protected set; }
        protected static string[] _keyWords = { "В", "ВЕЩ", "ВСТАВИТЬ", "ВЫБРАТЬ", "ВЫВОД", "ГДЕ", "ДИН", "ДЛЯ", "ЕСЛИ", "И", "ИЗ", "ИЛИ", "ИНАЧЕ", "КЛЮЧ", "ЛОГ", "МОДИФ", "НАИБ", "НАИМ", "НЕ", "СВЯЗЬ", "СОЗДАТЬ", "СРЕДНЕЕ", "СТР", "СЧЕТЧИК", "ТАБЛИЦУ", "ТО", "УДАЛИТЬ" };
        protected static string[] _expWords = { "В", "ВЫВОД", "ДЛЯ", "ЕСЛИ", "ИЗ", "СОЗДАТЬ" };
        protected static string[] _dataTypeWords = { "ДИН", "ВЕЩ", "ЛОГ", "СТР" };
        protected static char[] _unimportantDelims = { ' ', '\t', '\r', '\n' };
        protected static string[] _operations = { "+", "-", "*", "/", "%" };
        protected Stack<int> _endLabels;
        #endregion

        public SCANER()
        {
            Console.WriteLine("Введите имя файла со скриптом. ");
            Console.WriteLine("Пустая строка - файл '..\\..\\..\\script.ssql': ");
            Console.Write("--> ");
            string path = Console.ReadLine();
            if (path.Length == 0)
                path = "..\\..\\..\\script.ssql";

            toReadFromFile(path);
            Tokens = new List<LEXEME>();
            Identifiers = new List<IDENTIFIER>();
            _endLabels = new Stack<int>();
        }

        public SCANER(SCANER scaner)
        {
            _text = scaner._text;
            Tokens = new List<LEXEME>();
            Identifiers = new List<IDENTIFIER>();
            _identifierIndex = -1;
            _endLabels = new Stack<int>();

            toMakeAnalysis();
            toDefineLexemesType();
            //toMakeLabels();
        }

        protected void toReadFromFile(string path)
        {
            StreamReader file = new StreamReader(Stream.Null);
            try
            {
                file = new StreamReader(path);
            }
            catch (FileNotFoundException exc)
            {
                throw exc;
            }

            _text = file.ReadToEnd().ToUpperInvariant();
        }

        public void toMakeAnalysis()
        {
            _text = _text.Trim().Replace("\t", "    ");

            if (_text[0] != '#' || _text[_text.Length - 1] != '#')
                throw new TranslatorException("ошибка в ограничении транзакции #..#");

            int num = -1;
            string token = @"";
            bool fl = false;
            char c;
            bool isStringLiteral = false;
            int j = -1;
            int line = 1, symb = 1, symb1 = 1;

            for (int i = 1; i < _text.Length - 1; i++)
            {
                if (j > i) continue;

                #region Comments and string literals
                if (_text[i] == '\"')
                {
                    isStringLiteral = !isStringLiteral;
                }

                if (_text[i] == '?' && !isStringLiteral)
                {
                    if (_text[i + 1] == '?')
                    {
                        j = _text.IndexOf("??", i + 1) + 1;
                        if (j <= 0) throw new TranslatorException("незакрытый комментарий (начало в строке " + line + ")");

                        for (int j0 = i + 2; j0 < j; j0++)
                        {
                            symb1++;
                            if (_text[j0] == '\n')
                            {
                                line++;
                                symb = symb1 = 1;
                            }
                        }
                    }
                    else
                    {
                        j = _text.IndexOf("\n", i + 1);
                        symb = symb1 = 1;
                    }

                    continue;
                }
                #endregion

                c = _text[i];
                if (c == '\n')
                {
                    line++;
                    symb1 = 1;
                }
                else
                {
                    symb1++;
                }

                if (_unimportantDelims.Contains(c))
                {
                    Tokens.Add(new LEXEME(++num, token, line, symb));
                    token = @"";
                    fl = true;
                }

                if (c == '[' || c == ']' || c == '(' || c == ')' || c == ',' || c == '!' || c == ';' || 
                    c == '*' || c == '/' || c == '%' || c == '^')
                {
                    Tokens.Add(new LEXEME(++num, token, line, symb));
                    Tokens.Add(new LEXEME(++num, c.ToString(), LexemeType.Разделитель, line, symb));
                    token = @"";
                    fl = true;
                }

                if (c == '>' || c == '<')
                {
                    Tokens.Add(new LEXEME(++num, token, line, symb));
                    token = @"";
                    string val = @"";
                    if (_text[i + 1] == '=')
                    {
                        val = c.ToString() + "=";
                        j = i + 2;
                    }
                    else
                        if ((_text[i + 1] == '>' && c == '<') || (_text[i + 1] == '<' && c == '>'))
                        {
                            val = c.ToString() + _text[i + 1].ToString();
                            j = i + 2;
                        }
                        else
                        {
                            val = c.ToString();
                            j = i + 1;
                        }
                    Tokens.Add(new LEXEME(++num, val, LexemeType.Сравнение, line, symb));
                    fl = true;
                }

                if (c == '=')
                {
                    if (_text[i + 1] == '=')
                    {
                        Tokens.Add(new LEXEME(++num, token, line, symb));
                        token = @"";
                        Tokens.Add(new LEXEME(++num, "==", LexemeType.Сравнение, line, symb));
                        j = i + 2;
                        fl = true;
                    }
                    else
                    {
                        if (_text[i - 1] != '=' && _text[i - 1] != '<' && _text[i - 1] != '>')
                            throw new TranslatorException("ошибка оператора сравнения");
                    }
                    fl = true;
                }


                if (c == ':')// && (_unimportantDelims.Contains(_text[i + 1]) || _text[i + 1] == '(' || _text[i + 1] == '-' || _text[i + 1] == '+' || Char.IsDigit(_text[i + 1]) || _text[i + 1] == '_'))
                {
                    Tokens.Add(new LEXEME(++num, token, line, symb));
                    token = @"";
                    Tokens.Add(new LEXEME(++num, _text[i].ToString(), LexemeType.Присваивание, line, symb));
                    fl = true;
                }

                if (c == '+' || c == '-')
                {
                    int j0 = i - 1;
                    for (; j0 >= 0; j0--)
                    {
                        if (_unimportantDelims.Contains(_text[j0])) continue;
                        break;
                    }

                    if (_text[j0] == ':' || _text[j0] == '(' || _text[j0] == ',')
                    {
                        Tokens.Add(new LEXEME(++num, "0", LexemeType.КонстантаВЕЩ, line, symb));
                        Tokens.Add(new LEXEME(++num, c.ToString(), LexemeType.Разделитель, line, symb));
                        token = @"";
                        fl = true;
                    }
                    else
                    {
                        if (token.Length > 0)
                        {
                            Tokens.Add(new LEXEME(++num, token, line, symb));
                        }
                        Tokens.Add(new LEXEME(++num, c.ToString(), LexemeType.Разделитель, line, symb));
                        token = @"";
                        fl = true;
                    }
                }

                #region Constants
                // константы real
                if (c == '.')
                {
                    if (token.Length > 0 && !Char.IsDigit(token[0]) && token[0] != '-')
                    {
                        Tokens.Add(new LEXEME(++num, token, line, symb));
                        Tokens.Add(new LEXEME(++num, c.ToString(), LexemeType.Разделитель, line, symb));
                        token = @"";
                        fl = true;
                    }
                }

                // константа string
                if (c == '"')
                {
                    int j0 = _text.IndexOf('"', i + 1);
                    Tokens.Add(new LEXEME(++num, _text.Substring(i, j0 - i + 1), line, symb));
                    i = j0;
                    fl = true;
                    isStringLiteral = !isStringLiteral;
                    token = @"";
                }
                #endregion

                if (!fl)
                {
                    token += c;
                }
                else
                {
                    //token="";
                    symb = symb1;
                    fl = false;
                }
            }

            if (isStringLiteral) throw new TranslatorException("незакрытая строковая константа");

            for (int i = 0; i < Tokens.Count; i++)
            {
                if (Tokens.ElementAt(i).Value == "")
                {
                    Tokens.RemoveAt(i);
                    i--;
                    for (int j0 = i + 1; j0 < Tokens.Count; j0++)
                    {
                        Tokens.ElementAt(j0).Number--;
                    }
                }
            }
        }

        public void toDefineLexemesType()
        {
            int keyWordCode = -1;
            for (int i = 0; i < Tokens.Count; i++)
            {
                LEXEME token = Tokens.ElementAt(i);

                if (i >0 && _operations.Contains(token.Value) && _operations.Contains(Tokens[i - 1].Value))
                    throw new TranslatorException("Следует задать два операнда для операции '" + Tokens[i-1].Value + "' в строке " + token.Line);


                if (token.Value == "!")
                {
                    Tokens[i - 1].Type = LexemeType.ИмяТаблицы;
                    Tokens[i + 1].Type = LexemeType.ИмяПоля;
                }

                if (token.Type != LexemeType.__) continue;

                if (_expWords.Contains(token.Value))
                {
                    _endLabels.Push(i);
                }

                if (_keyWords.Contains(token.Value))
                {
                    token.Type = LexemeType.КлючевоеСлово;
                    if (token.Value == "ТАБЛИЦУ" || token.Value == "КЛЮЧ" ||
                        token.Value == "В" || token.Value == "ИЗ") keyWordCode = 1;
                    if (token.Value == "СВЯЗЬ") keyWordCode = 3;
                }

                if (token.Value == ".")
                {
                    token.Type = LexemeType.КонецВыражения;
                    keyWordCode = -1;
                }

                if (token.Value == ":") token.Type = LexemeType.Присваивание;

                if (token.Value == "ИСТИНА" || token.Value == "ЛОЖЬ")
                {
                    token.Type = LexemeType.КонстантаЛОГ;
                }
                if (token.Value[0] == '"')
                {
                    token.Type = LexemeType.КонстантаСТР;
                }
                if (Char.IsDigit(token.Value[0]) || token.Value[0] == '-')
                {
                    for (int j0 = 1; j0 < token.Value.Length; j0++)
                    {
                        if (token.Value[j0] != '.' && !Char.IsDigit(token.Value[j0]))
                            throw new TranslatorException("неверная константа [" + token.Value + "]");
                    }

                    token.Type = LexemeType.КонстантаВЕЩ;
                }

                if (token.Value[0] == '_')
                {
                    if (token.Value[1] == '_')
                    {
                        token.Type = LexemeType.ИмяКонстанты;
                    }
                    else
                    {
                        token.Type = LexemeType.ИмяПеременной;
                    }
                }

                if (token.Type == LexemeType.__)
                {
                    switch (keyWordCode)
                    {
                        case 1:
                            {
                                token.Type = LexemeType.ИмяТаблицы;
                                if (Tokens[i + 1].Value != ",")
                                    keyWordCode = 2;
                                break;
                            }
                        case 2:
                            {
                                token.Type = LexemeType.ИмяПоля;
                                //keyWordCode = -1;
                                break;
                            }
                        case 3:
                            {
                                token.Type = LexemeType.ИмяТаблицы;
                                keyWordCode = 4;
                                break;
                            }
                        case 4:
                            {
                                token.Type = LexemeType.ИмяПоля;
                                keyWordCode = 3;
                                break;
                            }
                        default:
                            {
                                throw new TranslatorException("Неизвестный символ: " + token.Value +
                                    " in " + token.Line + "; " + token.Symbol);
                            }
                    }
                }

                #region table id/Const
                if (token.Type.ToString().StartsWith("Константа"))
                {
                    DataType dt = DataType.СТР;
                    String l = token.Type.ToString().Substring("Константа".Length, 3);
                    dt = toDefineDataType(l);
                    int n0 = isListContainsID(token);
                    if (n0 == -1)
                    {
                    	AddIdentifier(i, " $$$ ", dt, token.Value, true);
                        token.Ref = _identifierIndex;
                    }
                    else
                    {
                        token.Ref = n0;
                    }
                }

                if (token.Type == LexemeType.ИмяКонстанты)
                {
                    DataType dt = DataType.СТР;
                    try
                    {
                        String l = Tokens.ElementAt(i - 1).Value;
                        dt = toDefineDataType(l);
                    }
                    catch (Exception)
                    {
                        throw new TranslatorException("Не указан тип данных для " + token.Value);
                    }                    

                    int n0 = isListContainsID(token);
                    if (n0 == -1)
                    {
                    	AddIdentifier(i, token.Value, dt, toDefineConstIDValue(i), true);
                        token.Ref = _identifierIndex;
                    }
                    else
                    {
                        if (Tokens.ElementAt(i + 1).Value == ":")
                            throw new TranslatorException("Константе не может быть присвоено новое значение");

                        token.Ref = n0;
                    }
                }

                if (token.Type == LexemeType.ИмяПеременной)
                {
                    int n0 = isListContainsID(token);
                    if (n0 == -1)
                    {
                        string val = @"";
                        if (Tokens.ElementAt(i + 1).Value != ":") val = " --- ";
                        else val = toDefineConstIDValue(i);

                        DataType dt = DataType.СТР;
                        string l = "";
                        try
                        {
                            l = Tokens.ElementAt(i - 1).Value;
                        }
                        catch (Exception)
                        {
                            throw new TranslatorException("Не указан тип данных для " + token.Value);
                        }

                        dt = toDefineDataType(l);

                        AddIdentifier(i, token.Value, dt, val, false);
                        token.Ref = _identifierIndex;
                    }
                    else
                    {
                        if (Tokens.ElementAt(i + 1).Value == ":")
                            Identifiers.ElementAt(n0).Value = toDefineConstIDValue(i);

                        token.Ref = n0;
                    }
                }
                #endregion

                if (i > 0 && (token.Type.ToString().StartsWith("Имя") || token.Type.ToString().StartsWith("Константа")) &&
                          (Tokens[i - 1].Type.ToString().StartsWith("Имя") || Tokens[i - 1].Type.ToString().StartsWith("Константа")))
                    throw new TranslatorException("Требуется разделитель после '" + Tokens[i-1].Value + "' в строке " + token.Line);


                if (token.Type == LexemeType.ИмяКонстанты || token.Type == LexemeType.ИмяПеременной ||
                    token.Type == LexemeType.ИмяПоля || token.Type == LexemeType.ИмяТаблицы)
                {
                    foreach (char c in token.Value)
                    {
                        if (c != '_' && !Char.IsLetterOrDigit(c)) throw new TranslatorException("странный символ [" + c + "] в " + token.Value + "\nСтрока " + token.Line + " символ " + token.Symbol);
                    }
                }

                if (token.Type == LexemeType.__) throw new TranslatorException("странный символ в " + token.Value);
            }
        }

        #region Таблица идентификаторов и констант
        protected DataType toDefineDataType(string l)
        {
            DataType dt = DataType.INFERENCE;
            if (l == "ВЕЩ") dt = DataType.ВЕЩ;
            if (l == "ЛОГ") dt = DataType.ЛОГ;
            if (l == "СТР") dt = DataType.СТР;
            if (l == "ДИН") dt = DataType.ДИН;
            return dt;
        }

        protected string toDefineConstIDValue(int number)
        {
            string s = @"";
            int F = 0;
            for (int j = number + 2; j < Tokens.Count; j++)
            {
                if (Tokens.ElementAt(j).Value == "(") F++;
                if (Tokens.ElementAt(j).Value == ";" || Tokens.ElementAt(j).Value == "," ||
                    (Tokens.ElementAt(j).Value == ")" && F == 0)) break;
                else s += Tokens.ElementAt(j).Value;
            }
            return s;
        }

        protected int isListContainsID(LEXEME token)
        {
            for (int i = 0; i < Identifiers.Count; i++)
            {
                if (Identifiers.ElementAt(i).Name == token.Value ||
                    Identifiers.ElementAt(i).Value == token.Value)
                    return i;
            }
            return -1;
        }

        public void toPrintIdentifiers()
        {
            Console.WriteLine("\n\n\nПеременные и константы");
            Console.WriteLine("--+--------+---+---+-------------------------------------------------------");
            Console.WriteLine("##+   Имя  +Тип+К/П+      Значение   (на конец выполнения программы)       ");
            Console.WriteLine("--+--------+---+---+-------------------------------------------------------");
            for (int i = 0; i < Identifiers.Count; i++)
            {
                IDENTIFIER id = Identifiers.ElementAt(i);
                Console.WriteLine("{0,-2}|{1,-8}|{2}| {3,-2}|{4,54}", id.Number, id.Name, id.Type, id.IsConst ? "+" : "-", id.Value);
            }
        }
        #endregion

        public void toPrintTokens()
        {
            Console.WriteLine("Лексемы");
            Console.WriteLine("----+--+--------------------------------------------------+-+--------------");
            Console.WriteLine(" ## +R#+                    Лексема                       +L+ Тип лексемы  ");
            Console.WriteLine("----+--+--------------------------------------------------+-+--------------");
            for (int i = 0; i < Tokens.Count; i++)
            {
                LEXEME lex = Tokens.ElementAt(i);
                Console.WriteLine("{0,-4}|{1,-2}|{2,-50}|{5}|{3,14}",
                    lex.Number, (lex.Ref == -1) ? "" : lex.Ref.ToString(), lex.Value, lex.Type,
                    (lex.EndLabel == 0) ? "" : lex.EndLabel.ToString(), lex.Line, lex.Symbol);
                //Console.WriteLine("|----|------------------------------|----------------|");
            }
        }

        #region Нужен parser'у и/или исполнителю
        public LEXEME toGetToken(int n)
        {
            if (n >= Tokens.Count)
            {
                Console.WriteLine("Достигнут конец программы.");
                throw new TranslatorException("EOP");
            }
            return Tokens.ElementAt(n);
        }

        public void toAddIterator(string name)
        {
            IDENTIFIER id = new IDENTIFIER(Identifiers.Count, name, DataType.ВЕЩ, "0", false);
            Identifiers.Add(id);
        }

        private int _identifierIndex;
        private int AddIdentifier(int tokenIndex, string name, DataType dt, string identifierValue, bool isConst)
        {
        	if (dt == DataType.INFERENCE)
        	{
        		dt = toInferenceType(identifierValue);
        		toInsertToken(dt.ToString(), LexemeType.КлючевоеСлово, tokenIndex);
        	}
        	IDENTIFIER newIdentifier = new IDENTIFIER(++_identifierIndex, name, dt, identifierValue, isConst);
        	Identifiers.Add(newIdentifier);
        	return _identifierIndex;
        }

        private void toInsertToken(string value, LexemeType type, int position)
        {
        	// add new
        	int num = Tokens[position].Number;
        	int line = Tokens[position].Line;
        	int symb = Tokens[position].Symbol;
        	LEXEME token = new LEXEME(num, value, type, line, symb);
        	Tokens.Insert(position, token);
        	
        	// move all the following
        	for (int i = position+1; i<Tokens.Count-1; i++)
        	{
        		Tokens[i].Number = Tokens[i+1].Number;
        		if (Tokens[i].Line == line) {
        			Tokens[i].Symbol += (value.Length + 1);
        		}
        	}
        	int lastIndex = Tokens.Count - 1;
        	Tokens[lastIndex].Number++;
        	if (Tokens[lastIndex].Line == line) {
        		Tokens[lastIndex].Symbol += (value.Length + 1);
        	}
        }
        private DataType toInferenceType(string value)
        {
        	if (value[0] == '"')
        		return DataType.СТР;
        	if (value == "ИСТИНА" || value == "ЛОЖЬ")
        		return DataType.ЛОГ;
        	double n;
        	if (double.TryParse(value, out n))
        		return DataType.ВЕЩ;
        	return DataType.ДИН;
        }

        public DataType toGetType(string valueOrName)
        {
        	if (valueOrName[0] == '_')
        	{
        		IDENTIFIER identif = toGetIdentifier(valueOrName);
        		return identif.Type;
        	}
        	return toInferenceType(valueOrName);
        }
        
        public IDENTIFIER toGetIdentifier(string name)
        {
            foreach (IDENTIFIER id in Identifiers)
            {
                if (id.Name == name) return id;
            }
            return null;
        }

        public IDENTIFIER toGetIdentifier(int number)
        {
            return Identifiers.ElementAt(number);
        }
        
        public bool isFirstOccurence(IDENTIFIER identifier, int currentTokenIndex)
        {
        	for(int i = 0; i < Tokens.Count; i++)
        	{
        		if (Tokens[i].Ref == identifier.Number)
        			return i == currentTokenIndex;
        	}
        	return false;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Scaner
{
    public enum LexemeType
    {
        КлючевоеСлово,
        ИмяПеременной,
        ИмяТаблицы,
        ИмяПоля,
        ИмяКонстанты,
        КонстантаВЕЩ,
        КонстантаЛОГ,
        КонстантаСТР,
        Разделитель,
        Присваивание,
        Сравнение,
        КонецВыражения,
        Унарный,
        __
    }

    public enum DelimType
    {
        ЗнакМатематическойОперации,
        ЗнакОператорСравнения,
        Присваивание,
        Скобка,
        КонецВыражения
    }

    public enum DataType
    {
    	INFERENCE,
    	ДИН,
        ВЕЩ,
        ЛОГ,
        СТР
    }
}

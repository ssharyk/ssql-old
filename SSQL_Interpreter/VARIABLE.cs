﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Interpreter
{
    public class VARIABLE
    {
        public string DataType { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsConst { get; set; }

        public VARIABLE(string dt, string nm, string val, bool isC)
        {
            DataType = dt;
            Name = nm;
            Value = val;
            IsConst = isC;
        }
    }
}

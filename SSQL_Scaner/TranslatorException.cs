﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Scaner
{
    public class TranslatorException :Exception
    {
        private string _message;

        public TranslatorException(string mes)
            : base(mes)
        {
            _message = mes;
        }
    }
}

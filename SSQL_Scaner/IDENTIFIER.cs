﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Scaner
{
    public class IDENTIFIER
    {
        public int Number { get; protected set; }
        public string Name { get; protected set; }
        public DataType Type { get; protected set; }
        public string Value { get; set; }
        public bool IsConst { get; protected set; }

        public IDENTIFIER(int num, string name, DataType type, string value, bool isConst)
        {
            Number = num;
            Name = name;
            Type = type;
            Value = value;
            IsConst = isConst;
        }

        public override string ToString()
        {
            //return "[" + Name + "  -->  " + Type + " ==== " + Value + "]";
            return Type.ToString() + " " + Name + " " + Value + " " + IsConst;
        }
    }
}

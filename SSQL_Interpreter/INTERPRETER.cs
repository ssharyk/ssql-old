using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SSQL_Scaner;

namespace SSQL_Interpreter
{
    public class INTERPRETER
    {
        #region ���� � ��������
        public List<string> PostfixForm { get; protected set; }
        public List<VARIABLE> Identifiers { get; protected set; }
        protected Dictionary<int, int> _labels;

        protected Stack<string> _stack;
        protected int _currentElement;
        public List<TABLE> Tables { get; protected set; }
        public List<LINK> Links { get; protected set; }
        private Stack<QueryInfo> _queries;

        private static string[] _operators = {   "+", "-", "*", "/", "%", "^",
                                                 "==", "<>", "><", ">=", "<=", "<", ">", 
                                                 "�", "���" };
        private static string[] _operatorsUnar = { "��", "���", "��", "LENGTH",
                                                   "����", "����", "�������" };
        private static string[] _commands = { "�����", ":", "�������_�", "�������_�", "�������_�", "�������_�", "��������", "�������", "����" };
        #endregion

        #region ���������������
        public INTERPRETER()
        {
            toReadFromFile();
            //toPrintPostfixForm();
            //toPrintIdTable();
            //toPrintLabels();
            //Console.ReadKey();
            _stack = new Stack<string>();

            Identifiers = new List<VARIABLE>();
            Tables = new List<TABLE>();
            Links = new List<LINK>();
            _queries = new Stack<QueryInfo>();
        }

        private void toReadFromFile()
        {
            Console.WriteLine("������� ��� ��� ����� ���: ");
            Console.WriteLine("�� ���������: ..\\..\\..\\program.rpn");
            Console.Write("--> ");
            string path = Console.ReadLine();
            if (path.Length == 0)
                path = "..\\..\\..\\program.rpn";
            StreamReader file = new StreamReader(new FileStream(path, FileMode.Open));

            int pfL = Int32.Parse(file.ReadLine());
            PostfixForm = new List<string>(pfL);
            for (int i = 0; i < pfL; i++)
            {
                PostfixForm.Add(file.ReadLine());
            }

            int lblL = Int32.Parse(file.ReadLine());
            _labels = new Dictionary<int, int>();
            for (int l = 0; l < lblL; l++)
            {
                string[] s = file.ReadLine().Split(' ');
                _labels[Int32.Parse(s[0])] = Int32.Parse(s[1]);
            }

            file.Close();
        }
        #endregion

        #region ����������
        public void toExecuteProgram()
        {
            string s;
            for (_currentElement = 0; _currentElement < PostfixForm.Count; _currentElement++)
            {
                s = PostfixForm.ElementAt(_currentElement);

                if (s == "$EOP")
                {
                    return;
                }

                if (_operators.Contains(s))
                {
                    string op1 = _stack.Pop(), op2 = _stack.Pop();
                    string res = toApplyOperation(s, op2, op1);
                    _stack.Push(res);

                    continue;
                }

                if (_operatorsUnar.Contains(s))
                {
                    string op1 = _stack.Pop();
                    string res = toApplyOperation(s, op1);
                    _stack.Push(res);

                    continue;
                }

                if (_commands.Contains(s))
                {
                    switch (s)
                    {
                        case ":":
                            {
                                string op1 = _stack.Pop(), op2 = _stack.Pop();
                                toExecuteAssignment(op2, op1);
                                break;
                            }

                        case "�������_�":
                            {
                                toExecuteTableCreation();
                                break;
                            }

                        case "�������_�":
                            {
                                toExecutePKCreation();
                                break;
                            }

                        case "�������_�":
                            {
                                toExecuteLinkCreation();
                                break;
                            }
                        case "�������_�":
                            {
                                toExecuteTableDelete();
                                break;
                            }

                        case "��������":
                            {
                                toExecuteInsertion();
                                _stack.Pop();
                                break;
                            }

                        case "�����":
                            {
                                toExecutePrint();
                                break;
                            }
                    }

                    continue;
                }

                // ����� ����� � ������
                if (s.IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm.ElementAt(_currentElement + 1) == "0" &&
                    PostfixForm.ElementAt(_currentElement + 2) == ":")
                {
                    QueryInfo info = new QueryInfo();
                    string tblName = s.Substring(0, s.IndexOf("_"));
                    info.CurrentTable = toGetTableByName(tblName);
                    if (info.CurrentTable == null)
                        throw new TranslatorException("������� " + tblName + " ��� �� �������");

                    info.IsInQuery = Int32.Parse(s.Substring(s.LastIndexOf("_") + 1));
                    info.QueryParameters = new List<QueryParameter>();
                    foreach (FIELD field in info.CurrentTable.Fields)
                        field.toClearValues();
                    string resName = info.CurrentTable.Name;
                    info.ResultTable = new TABLE(resName);

                    _queries.Push(info);

                    if (info.IsInQuery == 1)
                    {
                        toInitQuerySelect();
                        if (_queries.Peek().CurrentTable.Name[0] == '$')
                            _queries.Peek().ResultTable.Name = _queries.Peek().CurrentTable.Name;
                        else
                            _queries.Peek().ResultTable.Name = "$" + _queries.Peek().CurrentTable.Name;

                        toPrepareOutputTable();
                        _stack.Push(info.CurrentTable.Name + "_ITERATOR_" + info.IsInQuery);
                        continue;
                        //_iterator--;                        
                    }
                    else
                    {
                        toInitQueryModify();
                        if (info.IsQueryFinish)
                        {
                            info.IsQueryFinish = false;
                            _queries.Pop();
                            continue;
                        }
                        toPrepareOutputTable();
                    }
                }

                if (s.IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm[_currentElement + 1].IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm[_currentElement + 2] == "1" &&
                    PostfixForm[_currentElement + 3] == "+" &&
                    PostfixForm[_currentElement + 4] == ":")
                {
                    _queries.Peek().Iterator++;
                    _currentElement += 4;
                    _stack.Push(_queries.Peek().CurrentTable.Name + "_ITERATOR_" + _queries.Peek().IsInQuery);
                    continue;
                }

                _stack.Push(s);
            }
        }

        #region ����������
        private string toApplyOperation(string zn, string op1, string op2)
        {
            string o1 = toGetValue(op1);
            string o2 = toGetValue(op2);

            string dt1 = toGetValueType(o1), dt2 = toGetValueType(o2);
            if (dt1 == dt2)
            {
                switch (dt1)
                {
                    case "���":
                        {
                            bool v1 = op1 == "������";
                            bool v2 = op2 == "������";

                            if (zn == "�")
                            {
                                return ((v1 && v2) ? "������" : "����");
                            }

                            if (zn == "���")
                            {
                                return ((v1 || v2) ? "������" : "����");
                            }
                            throw new TranslatorException("���������� ��������� �������� " + zn + " � ���������� ���������");
                        }

                    case "���":
                        {
                            if (zn == "==") return (o1 == o2) ? "������" : "����";
                            if (zn == "<>") return (o1 != o2) ? "������" : "����";
                            if (zn == "+") return o1.Substring(0, o1.Length-1) + o2.Substring(1);                            
                            throw new TranslatorException("���������� ��������� �������� " + zn + " � ��������� ���������");
                        }

                    case "���":
                        {
                            double d1 = -11.1, d2 = -22.2;
                            try
                            {
                                o1 = o1.Replace('.', ',');
                                o2 = o2.Replace('.', ',');

                                d1 = double.Parse(o1);
                                d2 = double.Parse(o2);
                            }
                            catch (FormatException)
                            {
                                throw new TranslatorException("���������� �������������� ����� ��� ����, ����� ��������� �������� " + zn);
                            }

                            switch (zn)
                            {
                                case "+":
                                    {
                                        return (d1 + d2).ToString();
                                    }

                                case "-":
                                    {
                                        return (d1 - d2).ToString();
                                    }

                                case "*":
                                    {
                                        return (d1 * d2).ToString();
                                    }
                                case "/":
                                    {
                                        if (Math.Abs(d2) < 0.0000001)
                                            throw new TranslatorException("������� �� 0");

                                        if ((d1 / d2).ToString() == "NaN")
                                            throw new TranslatorException("�������� ���������������� ��� ������� " + d1 + " / " + d2);
                                        return (d1 / d2).ToString();
                                    }

                                case "%":
                                    {
                                        if (Math.Abs(d2) < 0.0000001)
                                            throw new TranslatorException("������� �� 0");

                                        if ((d1 / d2).ToString() == "NaN")
                                            throw new TranslatorException("�������� ���������������� ��� ������� " + d1 + " / " + d2);

                                        return (d1 % d2).ToString();
                                    }

                                case "^":
                                    {
                                        try
                                        {
                                            return Math.Pow(d1, d2).ToString();
                                        }
                                        catch (Exception)
                                        {
                                            throw new TranslatorException("�� ������� ��������� �������� ���������� � �������");
                                        }
                                    }


                                case ">":
                                    {
                                        return (d1 > d2) ? "������" : "����";
                                    }
                                case ">=":
                                    {
                                        return (d1 >= d2) ? "������" : "����";
                                    }
                                case "<":
                                    {
                                        return (d1 < d2) ? "������" : "����";
                                    }
                                case "<=":
                                    {
                                        return (d1 <= d2) ? "������" : "����";
                                    }
                                case "<>":
                                    {
                                        return (d1 != d2) ? "������" : "����";
                                    }
                                case "==":
                                    {
                                        return (d1 == d2) ? "������" : "����";
                                    }
                            }

                            throw new TranslatorException("���������� ��������� �������� " + zn + " � ������������ ���������");
                        }
                }
            }

            if (toGetValueType(o1) == "���" && toGetValueType(o2) == "���" && zn == "*")
            {
                uint count;
                if (!UInt32.TryParse(o2, out count))
                    throw new TranslatorException("���������� ��������� ������ " + o1 + " " + o2 + " ���");
                
                string s = "";
                for (uint x = 0; x < count; x++)
                    s += o1.Substring(1, o1.Length - 2);
                return "\"" + s + "\"";
            }
            throw new TranslatorException("�������� " + op1 + " � " + op2 + " ������ ����� ��� ���������� " + zn);
        }

        private string toApplyOperation(string zn, string op)
        {
            //Console.WriteLine("\tApply {0}({1})", zn, op);
            switch (zn)
            {
                case "��":
                    {
                        string o = toGetValue(op);
                        string dt = toGetValueType(o);
                        if (dt == "���")
                        {
                            if (o == "������") return "����";
                            if (o == "����") return "������";
                            throw new TranslatorException("���������� ��������� �� � ��������� ������������� ����: " + o);
                        }
                        throw new TranslatorException("���������� ��������� �� � ��������� ������������� ����: " + o);
                    }

                case "��":
                    {
                        int lbl = Int32.Parse(op.Substring(1));
                        _currentElement = _labels[lbl] - 1;
                        return PostfixForm.ElementAt(_currentElement);
                    }

                case "���":
                    {
                        int lbl = Int32.Parse(op.Substring(1));

                        if (_stack.Peek() == "����")
                        {
                            for (int j0 = _currentElement + 1; j0 < _labels[lbl] - 1; j0++)
                            {
                                string s1 = PostfixForm.ElementAt(_currentElement);
                                if (!_commands.Contains(s1) && !_operators.Contains(s1) && !_operatorsUnar.Contains(s1))
                                    _stack.Push(s1);
                            }
                            _currentElement = _labels[lbl] - 1;
                        }
                        else
                        {
                            if (_queries.Count > 0 && PostfixForm[_currentElement - 3] != "LENGTH")
                            {
                                switch (_queries.Peek().IsInQuery)
                                {
                                    case 1:
                                        {
                                            // � ������ ����� ��� � ��������� @
                                            int j = PostfixForm.LastIndexOf("���", _currentElement - 1);
                                            if (PostfixForm[j - 3] == "LENGTH")
                                                toPrintPerformedRecord();
                                            break;
                                        }

                                    case 2: { _queries.Peek().IsNeedPrintRecord = true; break; }
                                    case 3:
                                        {
                                            //Console.WriteLine("delete with _iterator = " + _queries.Peek().Iterator);

                                            /// TODO: �������
                                            toPrintPerformedRecord();

                                            TABLE table = _queries.Peek().CurrentTable;
                                            //table.Records.RemoveAt(_queries.Peek().Iterator);
                                            table.Records[_queries.Peek().Iterator] = null;
                                            break;
                                        }
                                }

                            }
                        }

                        break;
                    }

                case "LENGTH":
                    {
                        //_isInQuery = 1; /// TOOD: 1-select, 2-modif
                        _stack.Push(PostfixForm[_currentElement - 1]);
                        //string tblName = PostfixForm.ElementAt(_currentElement - 1);
                        //tblName = tblName.Substring(0, tblName.IndexOf("_ITERATOR_"));

                        //TABLE table = toGetTableByName(tblName);
                        TABLE table = _queries.Peek().CurrentTable;

                        if (_queries.Peek().IsInQuery != 0 && _queries.Peek().Iterator < table.Counter)
                        {
                            //Console.WriteLine("_iterator = {0}", _iterator);
                            if (_queries.Peek().IsInQuery == 1)
                            {
                                toInitQuerySelect();
                            }
                            else
                            {
                                //Console.WriteLine("MODIFICATION:");
                                toUpdateParameters();
                                //Console.WriteLine("\r\n\r\n");
                            }
                        }
                        else
                        {
                            string queryType = "";
                            QueryInfo info = _queries.Pop();
                            switch (info.IsInQuery)
                            {
                                case 1: { queryType = "�������"; break; }
                                case 2: { queryType = "�����������"; break; }
                                case 3: { queryType = "��������"; break; }                            
                            }
                            /// TODO: �������
                            //info.ResultTable.toPrint();
                            Console.WriteLine("����� ���������� ����� � ���� ������� �� {0}: {1}\r\n\r\n",
                                queryType, info.NumberOfPerformedRecords);                            

                            if (info.IsInQuery == 1)
                            {
                                // ����� ����� ��� ����������� ���������
                                bool isAgg = false;
                                for (int ai = _currentElement + 2; ai < info.QueryBody; ai++)
                                {
                                    if (PostfixForm[ai] == "����" || PostfixForm[ai] == "����" || PostfixForm[ai] == "�������")
                                    {
                                        isAgg = true;
                                        string fldName = PostfixForm[ai - 1];
                                        TABLE tbl=toGetTableByName( PostfixForm[ai - 2]);                                       
                                        FIELD field = tbl.toGetFieldByName(fldName);

                                        string v = "";
                                        if (PostfixForm[ai] == "�������")
                                            v = field.AvgValue.ToString();
                                        else
                                            v = (PostfixForm[ai] == "����") ? field.MaxValue.ToString() : field.MinValue.ToString();

                                        Console.WriteLine("�������� {0}({1}) = {2}", PostfixForm[ai], fldName, v);
                                    }
                                }

                                // �������� �������-������������
                                for (int jt = 0; jt < Tables.Count; jt++)
                                {
                                    if (Tables[jt].Name[0] == '$')
                                    {
                                        Tables.RemoveAt(jt);
                                        break;
                                    }
                                }
                                if (isAgg) Console.WriteLine("\r\n\r\n");
                            }

                            for (int jd = 0; jd < table.Records.Count; jd++)
                            {
                                if (table.Records[jd] == null)
                                {
                                    table.Records.RemoveAt(jd);
                                    jd--;
                                }
                            }
                            table.Counter = table.Records.Count;

                            //_isInQuery = 0;
                            //_queryBody = PostfixForm.Count;
                            //_iterator = 0;
                            //_numberOfPerformedRecords = 0;
                            //_queryParameters.Clear();
                            //_currentTable = "";

                            int lblN = Int32.Parse(PostfixForm[_currentElement + 2].Substring(1));
                            _currentElement = _labels[lblN];

                            return PostfixForm[_currentElement];
                        }

                        //Console.WriteLine("Length of {0} is {1}    ITER: {2}", tblName, table.Records.Count, _iterator);
                        return table.Records.Count.ToString();
                    }

                case "����":
                case "����":
                case "�������":
                    {
                        //TABLE table = _queries.Peek().CurrentTable;
                        string tblName = PostfixForm[_currentElement - 2];
                        TABLE table = toGetTableByName(tblName);

                        string fldName = op;
                        FIELD field = table.toGetFieldByName(fldName);
                        if (field == null)
                            throw new TranslatorException("���� " + fldName + " �� ���������� � ������� " + table.Name);
                        int fldNumber = field.Number;

                        //Console.WriteLine("!!!!!!!!!!!!! aggr: {0} ( {1}: {2} ).", zn, fldNumber, fldName);

                        string aggr = _queries.Peek().CurrentTable.toApplyAggregate(fldNumber, zn, _queries.Peek().Iterator, _queries.Peek().NumberOfPerformedRecords - 1, table);
                        //Console.WriteLine("\tNow aggr = " + aggr);

                        return aggr;
                    }
            }
            return "";
        }
        #endregion

        #region ���������� ������
        private void toExecuteAssignment(string to, string from)
        {
            // UPDATE table SET field = value
            if (_queries.Count > 0 && _queries.Peek().IsInQuery == 2 && to.IndexOf("_ITERATOR_") == -1)
            {
                // ������ � �������
                TABLE table = _queries.Peek().CurrentTable;
                int num = table.toGetFieldNumber(to);


                string valUpd = from;
                if (_queries.Count > 1 && from[0] == '_' && from[1] != '_')
                {
                    // UPDATE ������ FOR-EACH
                    Stack<QueryInfo> stack = new Stack<QueryInfo>(_queries);
                    QueryInfo qi = stack.Pop();
                    QueryParameter par = null;
                    do
                    {
                        par = qi.toGetParameterByVariableName(from);
                        if (par != null)
                        {
                            valUpd = par.FieldValue;
                        }
                        qi = stack.Pop();
                    } while (stack.Count > 0);

                    if (qi == null)
                        throw new TranslatorException("����� ���������� ���");
                }

                table.Records[_queries.Peek().Iterator][num] = valUpd;

                // ���������� ������ ���-���
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.FieldName == to)
                    {
                        par.FieldValue = valUpd;
                        break;
                    }
                }

                /// TODO: �������
                toPrintPerformedRecord();

                return;
            }

            VARIABLE var = toGetVariableByName(to);
            string val = toGetValue(from);
            if (var == null)
            {
                string dt = _stack.Pop();
                if (dt == DataType.���.ToString() ||
                    toGetValueType(val) == dt)
                {
                    var = new VARIABLE(dt, to, val, to[1] == '_');
                    Identifiers.Add(var);
                }
                else
                {
                    if (to[0] != '_')
                    {
                        string tblName = to;
                        TABLE table = toGetTableByName(tblName);
                        if (table == null)
                            throw new TranslatorException("������� " + tblName + " ������ ������������ � �������, ��� ��� �� ��� �� ����������");

                        string fldName = from;
                    }
                    else
                    {
                        throw new TranslatorException("�������������� ����� ������ ��� ������������: ���������� ��������� " + dt + " " + to + " �������� " + toGetValueType(val) + " " + from);
                    }
                }
            }
            else
            {
            	if (var.DataType == DataType.���.ToString() ||
	           	    toGetValueType(val) == var.DataType)
                {
                    var.Value = val;
                }
                else
                {
                    throw new TranslatorException("���������� ���������� ����� ��� ������������ �������� " + val + " � ���������� " + to + " ���� " + var.DataType);
                }
            }
        }

        private void toExecuteTableCreation()
        {
            int n = Int32.Parse(_stack.Pop()) - 2;
            string tblName = _stack.ElementAt(n + 1);
            TABLE test = toGetTableByName(tblName);
            if (test != null)
                throw new TranslatorException("������� " + tblName + " ��� ����������");

            TABLE tbl = new TABLE(tblName);

            for (int i = 0; i < n; i += 2)
            {
                string nm = _stack.ElementAt(n - (i + 1));
                string dt = _stack.ElementAt(n - (i + 0));
                tbl.toAddField(dt, nm);
            }
            for (int i = 0; i < n + 2; i++)
            {
                _stack.Pop();
            }
            Tables.Add(tbl);
        }

        private void toExecutePKCreation()
        {
            string tblName = _stack.Pop();
            TABLE table = toGetTableByName(tblName);
            if (table == null)
            {
                string message = "������� " + tblName + " ��� �� ����������!\n";
                message += "�������� ��, ������ ��� ������������ ����.";
                throw new TranslatorException(message);
            }

            int parameters = Int32.Parse(_stack.Pop());
            for (int i = 0; i < parameters; i++)
            {
                string fld = _stack.Pop();
                table.toMarkPK(fld);
            }
        }

        private void toExecuteInsertion()
        {
            int n = Int32.Parse(_stack.Pop());
            string tbl = _stack.ElementAt(n - 1);
            TABLE table = toGetTableByName(tbl);
            if (table == null)
                throw new TranslatorException("������� " + tbl + " ��� �� �������");

            if (n - 1 != table.Fields.Count)
                throw new TranslatorException("�������������� ���������� ����� � ������� � ����������� �������");

            // �������
            int lines = table.Records.Count;
            table.Records.Add(new Dictionary<int, string>());
            bool isNotUnique = true;
            string key = "]";

            for (int i = n - 2; i >= 0; i--)
            {
                // �������� ����� ������
                string val = _stack.Pop();
                FIELD fld = table.Fields.ElementAt(i);
                string dt = toGetValueType(val);
                if (dt != fld.Type && (!(dt == "���" && fld.Type == "���")))
                    throw new TranslatorException("�������������� ����� ������ �������� " +dt+" "+ val + " � ���� " + fld.Type + " " + fld.Name);

                // �������� ��������� PK
                //bool isNU = table.isNotUnique(i, val);
                //isNotUnique &= isNU;
                //if (isNU) key = (" " + val + " ") + key;

                if (!isMigrationCorrect(table, fld, val))
                    throw new TranslatorException("������ �������� ������ �� ��������� ���� " + fld.Name +
                        " = " + val + ", ��� ��� ���������� �������� ������");

                table.Records.ElementAt(lines)[i] = val;
            }

            isNotUnique = table.isNotUnique();

            if (isNotUnique)
            {
                key = table.toGetPK(table.Records.Count - 1);
                isNotUnique = false;
                throw new TranslatorException("������ � ������ " + key + " ��� ���������� � ������� " + table.Name);
            }

            table.Counter++;
        }

        private void toExecutePrint()
        {
            int parameters = Int32.Parse(_stack.Pop());
            Console.Write(">>> ");

            for (int i = parameters - 1; i >= 0; i--)
            {
                string op = _stack.ElementAt(i);
                string s = toGetValue(op);
                if (s[0] == '"')
                {
                    s = s.Substring(1, s.Length - 2);
                }
                Console.Write(s);
            }
            Console.WriteLine();
        }

        private void toExecuteTableDelete()
        {
            string tblname = _stack.Pop();
            TABLE table = toGetTableByName(tblname);
            if (table == null)
                throw new TranslatorException("������� " + tblname + " �� ����������");
            Tables.Remove(table);
        }


        #region �����
        private void toExecuteLinkCreation()
        {
            string fkF = _stack.Pop();
            string fkT = _stack.Pop();
            string pkF = _stack.Pop();
            string pkT = _stack.Pop();
            string link_str = pkT + " ! " + pkF + "    <--->    " + fkT + " ! " + fkF;

            // ��������� ����
            TABLE tableF = toGetTableByName(fkT);
            if (tableF == null)
                throw new TranslatorException("���������� ������� ����� " + link_str + " ��� ��� ������� " + fkT + " ��� �� ����������");
            FIELD fieldF = tableF.toGetFieldByName(fkF);
            if (fieldF == null)
                throw new TranslatorException("���������� ������� ����� " + link_str + " ��� ��� ���� " + fkF + " ��� �� ���������� � ������� " + fkT);

            // ��������� ����
            TABLE tableP = toGetTableByName(pkT);
            if (tableP == null)
                throw new TranslatorException("���������� ������� ����� " + link_str + " ��� ��� ������� " + pkT + " ��� �� ����������");
            FIELD fieldP = tableP.toGetFieldByName(pkF);
            if (fieldP == null)
                throw new TranslatorException("���������� ������� ����� " + link_str + " ��� ��� ���� " + pkF + " ��� �� ���������� � ������� " + pkT);

            Links.Add(new LINK(tableP, fieldP, tableF, fieldF));

            // 1) ������ ���������� �������� ���������� �����
            List<string> allowed = toGetAllowedValues(tableP, fieldP.Number);

            // 2) �������� ��������
            foreach (Dictionary<int, string> rec in tableF.Records)
            {
                if (!allowed.Contains(rec[fieldF.Number]))
                    throw new TranslatorException("������ �������� �����: ������ ������� �����" +
                        " ��� ��� � ���� " + tableP.Name + " ! " + fieldP.Name + " ��� ����� " + rec[fieldF.Number]);
            }
        }

        private bool isMigrationCorrect(TABLE table, FIELD field, string val)
        {
            foreach (LINK link in Links)
            {
                if (link.FK_Table == table && link.FK_Field == field)
                {
                    int fldNumber = link.PK_Field.Number;
                    List<string> allowed = toGetAllowedValues(link.PK_Table, fldNumber);
                    if (!allowed.Contains(val)) return false;
                }
            }
            return true;
        }

        private List<string> toGetAllowedValues(TABLE table, int fldNumber)
        {
            List<string> allowed = new List<string>();
            foreach (Dictionary<int, string> rec in table.Records)
            {
                allowed.Add(rec[fldNumber]);
            }
            return allowed;
        }
        #endregion

        #region ������
        private void toInitQuerySelect()
        {
            int cur = 0;
            int i = _currentElement;
            //_iterator=0;
            _queries.Peek().QueryParameters.Clear();

            string[] aggF = { "����", "����", "�������" };
            // ������� �� ������ ����������
            for (; i < PostfixForm.Count; i++)
            {
                if (PostfixForm[i][0] == '@')
                {
                    cur++;
                    if (cur == 2)
                    {
                        break;
                    }
                }
            }
            i += 2;

            string v_nm, f_nm, zn, tbl;
            List<TABLE> usedTables = new List<TABLE>();
            for (int j = i; j < PostfixForm.Count; j += 5)
            {
                zn = PostfixForm[j + 4];

                if (zn != ":" && !aggF.Contains(zn)) //|| !Char.IsLetter(PostfixForm[j+1][0]))
                {
                    _queries.Peek().QueryBody = j;
                    break;
                }

                if (zn == ":")
                {
                    if (PostfixForm[j].IndexOf("_ITERATOR_") != -1)
                    {
                        _queries.Peek().QueryBody = j;
                        break;
                    }

                    v_nm = PostfixForm[j + 1];
                    tbl = PostfixForm[j + 2];
                    TABLE table = toGetTableByName(tbl);
                    if (table == null)
                        throw new TranslatorException("������� " + tbl + " ��� �� ����������");
                    if (!usedTables.Contains(table))
                        usedTables.Add(table);
                    f_nm = PostfixForm[j + 3];
                    _queries.Peek().QueryParameters.Add(new QueryParameter(table, v_nm, f_nm, toGetFieldValue(tbl, f_nm), false));
                }
                if (aggF.Contains(zn)) j++;
            }


            if (usedTables.Count > 1)
            {
                /// TODO: ��������� ������� => � ��� ���� ����������, �� ����� �� ����
                for (int j = _currentElement; j < _queries.Peek().QueryBody + 5; j++)
                {
                    foreach (TABLE table in usedTables)
                    {
                        if (table.Name == PostfixForm[j] && !aggF.Contains(PostfixForm[j+2]))
                            PostfixForm[j] = "$1";

                        int ind = PostfixForm[j].IndexOf("_ITERATOR_");
                        if (ind != -1)
                        {
                            string t = PostfixForm[j].Substring(0, ind);
                            if (table.Name == t)
                                PostfixForm[j] = "$1" + "_ITERATOR_" + "1";
                        }
                    }
                }

                TABLE join = new TABLE("$1");
                // ����
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    FIELD field = par.Table.toGetFieldByName(par.FieldName);
                    join.toAddField(field.Type, par.FieldName);
                }

                // ������
                int numberOfRepeat = 1;
                foreach (TABLE table in usedTables)
                    numberOfRepeat *= table.Records.Count;
                join.Records = new List<Dictionary<int, string>>(numberOfRepeat);
                join.Counter = numberOfRepeat;
                for (int j = 0; j < numberOfRepeat; j++)
                    join.Records.Add(new Dictionary<int, string>());

                int incr = 1;
                foreach (TABLE table in usedTables)
                {
                    numberOfRepeat /= table.Records.Count;

                    foreach (QueryParameter par in _queries.Peek().QueryParameters)
                    {
                        if (par.Table == table)
                        {
                            int fldNumberIn = table.toGetFieldNumber(par.FieldName);
                            int fldNumberOut = join.toGetFieldNumber(par.FieldName);

                            // ������ � �������� �������
                            int line = 1 - incr;
                            for (int iter = 0; iter < table.Records.Count; iter++)
                            {
                                string val = table.Records[iter][fldNumberIn];

                                for (int times = 0; times < numberOfRepeat; times++)
                                {
                                    line += incr;
                                    if (incr > 1 && line > join.Records.Count)
                                        line = iter + 1;

                                    join.Records[line - 1][fldNumberOut] = val;
                                }
                            }
                        }
                    }

                    incr *= numberOfRepeat;
                    numberOfRepeat *= table.Records.Count;
                }


                Tables.Add(join);
                _queries.Peek().CurrentTable = join;
            }
        }

        private void toPrintPerformedRecord()
        {
            _queries.Peek().NumberOfPerformedRecords++;
            _queries.Peek().ResultTable.Records.Add(new Dictionary<int,string>());

            Console.Write("{0,2}", _queries.Peek().NumberOfPerformedRecords);
            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                Console.Write("|{0,15}", par.FieldValue);
                int fldNumber=_queries.Peek().ResultTable.toGetFieldNumber(par.FieldName);
                _queries.Peek().ResultTable.Records[_queries.Peek().NumberOfPerformedRecords-1][fldNumber] = par.FieldValue;
            }
            Console.WriteLine();
        }

        private void toInitQueryModify()
        {
            // ������� �����������
            int i = _currentElement;
            if (_queries.Peek().CurrentTable.Records.Count == 0)
            {
                Console.WriteLine("� ���� ������� ������� �����\r\n\r\n");
                _queries.Peek().IsQueryFinish = true;

                int lbl = Int32.Parse(PostfixForm[_currentElement + 6].Substring(1));
                _currentElement = _labels[lbl] - 1;
                _queries.Peek().QueryBody = PostfixForm.Count;

                return;
            }



            // ������� �� ������ ����������
            for (; i < PostfixForm.Count; i++)
            {
                if (PostfixForm[i][0] == '@')
                {
                    break;
                }
            }
            i += 1;

            do
            {
                i++;
                string s = PostfixForm[i];
                if (Char.IsLetter(s[0]) && s.IndexOf("_ITERATOR_") == -1)
                {
                    _queries.Peek().QueryParameters.Add(new QueryParameter(_queries.Peek().CurrentTable, "_" + s, s, toGetFieldValue(_queries.Peek().CurrentTable.Name, s), false));
                }

                if (s[0] == '@')
                {
                    if (PostfixForm[i + 1] == "��") break;
                    i++;
                }
            } while (true);
            _queries.Peek().QueryBody = PostfixForm.Count;


            // �������� ���� ������� �� �������
            if (_queries.Peek().IsInQuery == 3 && _queries.Peek().QueryParameters.Count == 0)
            {
                _queries.Peek().IsQueryFinish = true;
                // ������� �������
                TABLE table = _queries.Peek().CurrentTable;

                int n = table.Records.Count;
                table.Records.RemoveRange(0, n);
                Console.WriteLine("����� ���������� ����� � ���� ������� �� �������� (������� �������): {0}\r\n\r\n", n);

                int lbl = Int32.Parse(PostfixForm[_currentElement + 6].Substring(1));
                _currentElement = _labels[lbl] - 1;

                // ������� �� ����� �������
                _queries.Peek().QueryBody = PostfixForm.Count;
            }
        }

        private void toUpdateParameters()
        {
            TABLE table = _queries.Peek().CurrentTable;

            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                int num = table.toGetFieldNumber(par.FieldName);
                par.FieldValue = table.Records[_queries.Peek().Iterator][num];
            }
        }
        #endregion
        #endregion
        #endregion

        #region ����� �������� ����������: ��������� � �������� ��������������� | ����� ������ ��
        private string toGetValue(string query)
        {
            if (Char.IsDigit(query[0]) || query[0] == '"' ||
               query[0] == '+' || query[0] == '-' ||
               query == "������" || query == "����")
            {
                return query;
            }

            if (query.IndexOf("_ITERATOR_") != -1)
            {
                VARIABLE v = toGetVariableByName(query);
                if (v == null)
                    throw new TranslatorException("���������� " + query + " ��� �� �������");
                return v.Value;
            }


            if (_queries.Count > 0 && _queries.Peek().IsInQuery == 1 && query[0] == '_')
            {
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.VariableName == query)
                    {
                        return par.FieldValue;
                    }
                }
            }

            if (_queries.Count > 0 && _queries.Peek().IsInQuery != 0 && Char.IsLetter(query[0]))
            {
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.FieldName == query)
                    {
                        return par.FieldValue;
                    }
                }
            }

            if (query[0] == '_' || query.IndexOf("_ITERATOR_") != -1)
            {
                VARIABLE v = toGetVariableByName(query);
                if (v == null)
                    throw new TranslatorException("���������� " + query + " ��� �� �������");
                return v.Value;
            }

            return "";
        }

        private VARIABLE toGetVariableByName(string name)
        {
            foreach (VARIABLE v in Identifiers)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }

        private string toGetValueType(string val)
        {
            if (val.Length == 0)
                throw new TranslatorException("�������� ���������� ������������.\r\n��������, ��� ���������� �� ������������� ��������");

            if (val.IndexOf("_ITERATOR_") != -1) return "���";
            if (val == "������" || val == "����") return "���";
            if (val[0] == '"' && val[val.Length - 1] == '"') return "���";
            if (Char.IsDigit(val[0]) || val[0] == '+' || val[0] == '-') return "���";

            throw new TranslatorException("��� �������� " + val + " �� ���������");
        }

        private TABLE toGetTableByName(string name)
        {
            foreach (TABLE table in Tables)
            {
                if (table.Name == name)
                    return table;
            }
            return null;
        }

        private string toGetFieldValue(string tblName, string fldName)
        {
            TABLE table = toGetTableByName(tblName);
            if (table == null)
                throw new TranslatorException("����� ������� ��� �� ����������");

            FIELD field = table.toGetFieldByName(fldName);
            if (field == null)
                throw new TranslatorException("���� " + fldName + " �� ���������� � ������� " + tblName);

            return table.Records[_queries.Peek().Iterator][field.Number];
        }
        #endregion

        #region �������
        private void toPrintPostfixForm()
        {
            Console.WriteLine("\r\n\r\nPOSTFIX FORM:");
            for (int i = 0; i < PostfixForm.Count; i++)
            {
                Console.WriteLine("\t{0} --> {1}", i, PostfixForm.ElementAt(i));
            }
            Console.WriteLine("# POSTFIX FORM\r\n\r\n");
        }

        public void toPrintIdTable()
        {
            Console.WriteLine("\r\n\r\nID/CONST:");
            foreach (VARIABLE v in Identifiers)
            {
                Console.WriteLine("\t" + v);
            }
            Console.WriteLine("# ID/CONST\r\n\r\n");
        }

        private void toPrintStack()
        {
            Console.WriteLine("STACK:");
            Stack<string> stack = new Stack<string>(_stack);
            while (stack.Count > 0)
            {
                Console.WriteLine("\t" + stack.Pop());
            }
            Console.WriteLine("# STACK");
        }

        private void toPrintLabels()
        {
            Console.WriteLine("\r\n\r\nLABELS:");
            for (int i = 0; i < _labels.Keys.Count; i++)
            {
                Console.WriteLine("\t<{0}> --> <{1}>", _labels.Keys.ElementAt(i), _labels[_labels.Keys.ElementAt(i)]);
            }
            Console.WriteLine("# LABELS\r\n\r\n");
        }

        public void toPrintDataBase()
        {
            Console.WriteLine("\n================");
            Console.WriteLine("���� ������");
            Console.WriteLine("================");

            Console.WriteLine("\t����� ������: " + Tables.Count);
            foreach (TABLE table in Tables)
            {
                table.toPrint();
            }

            Console.WriteLine("\r\n\r\n\t����� ������: " + Links.Count);
            foreach (LINK link in Links)
            {
                Console.WriteLine(link);
            }
            Console.WriteLine("================\n\n");
        }

        private void toPrepareOutputTable()
        {
            // ���������� ������� ��� ������
            Console.Write("\r\n##");
            int n = 0;
            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                _queries.Peek().ResultTable.toAddField(toGetValueType(par.FieldValue), par.FieldName);
                if (!par.IsAggregate)
                {
                    Console.Write("|{0,15}", par.FieldName);
                    n++;
                }
            }
            Console.WriteLine();
            for (int parC = 0; parC < n; parC++)
                for (int l = 0; l < 15; l++)
                    Console.Write("-");
            Console.WriteLine("----");
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Interpreter
{
    public class TABLE
    {
        #region Поля и свойства
        public string Name {get; set;}
        public List<FIELD> Fields { get; private set; }
        public List<Dictionary<int, string>> Records { get; set; }
        public int Counter { get; set; }
        #endregion

        #region Конструкторы
        public TABLE()
        {
            Fields = new List<FIELD>();
            Records = new List<Dictionary<int, string>>();
        }

        public TABLE(string nm)
        {
            Name = nm;
            Fields = new List<FIELD>();
            Records = new List<Dictionary<int, string>>();
        }
        #endregion

        #region Структура таблицы
        public void toAddField(string dt, string nm)
        {
            FIELD field = new FIELD(Fields.Count, dt, nm, false);
            Fields.Add(field);
        }

        public void toMarkPK(string fieldName)
        {
            foreach (FIELD field in Fields)
            {
                if (field.Name == fieldName)
                {
                    field.IsPK = true;
                    return;
                }
            }

            string message = "Поля " + fieldName + " не определено в таблице " + Name;
            throw new SSQL_Scaner.TranslatorException(message);
        }

        public string toGetPK(int rec)
        {
            string key="[";
            foreach (FIELD field in Fields)
            {
                if (field.IsPK)
                    key += (" " + Records[rec][field.Number] + " ");
            }
            return key+"]";
        }

        public bool isNotUnique()
        {
            bool isNU = true;;
            Dictionary<int, string> lastRecord = Records[Records.Count - 1];

            for (int i = 0; i < Records.Count - 1; i++)
            {
                isNU=true;
                foreach (int key in lastRecord.Keys)
                {
                    if (!Fields[key].IsPK) continue;

                    if (Records[i][key] != lastRecord[key])
                    {
                        isNU = false;
                        break;
                    }
                }

                if (isNU) return true;
            }

            return false;
        }

        public FIELD toGetFieldByName(string fldName)
        {
            foreach (FIELD field in Fields)
            {
                if (field.Name == fldName)
                    return field;
            }
            return null;
        }

        public int toGetFieldNumber(string fldName)
        {
            foreach (FIELD field in Fields)
            {
                if (field.Name == fldName)
                    return field.Number;
            }
            return -1;
        }
        #endregion

        #region Работа с записями
        public string toApplyAggregate(int fldNumber, string func)
        {
            double val=0;
            switch (func)
            {
                case "НАИБ": { val = Double.MinValue; break; }
                case "НАИМ": { val = Double.MaxValue; break; }
                case "СРЕДНЕЕ": { val = 0; break; }
            }

            foreach (Dictionary<int, string> rec in Records)
            {
                double d = Double.Parse(rec[fldNumber].Replace('.', ','));
                if (func == "НАИМ" && d < val) { val = d; }
                if (func == "НАИБ" && d > val) { val = d; }
                if (func == "СРЕДНЕЕ") { val += d; }
            }

            switch (func)
            {
                case "НАИБ": { Fields[fldNumber].MaxValue = val; return val.ToString(); }
                case "НАИМ": { Fields[fldNumber].MinValue = val; return val.ToString(); }
                case "СРЕДНЕЕ": { Fields[fldNumber].AvgValue = val / Records.Count; return (val / Records.Count).ToString(); }
            }
            return "";
        }

        public string toApplyAggregate(int fldNumber, string func, int lineNumber, int perfCount, TABLE dest)
        {
            int in_this_t = toGetFieldNumber(dest.Fields[fldNumber].Name);

            string v = Records[lineNumber][in_this_t].Replace('.', ',');
            double d = Double.Parse(v);
            switch (func)
            {
                case "НАИБ":
                    {
                        if (d > dest.Fields[fldNumber].MaxValue)
                        {
                            dest.Fields[fldNumber].MaxValue = d;
                            //return d.ToString();
                        }
                        return dest.Fields[fldNumber].MaxValue.ToString();
                    }

                case "НАИМ":
                    {
                        if (d < dest.Fields[fldNumber].MinValue)
                        {
                            dest.Fields[fldNumber].MinValue = d;
                            //return d.ToString();
                        }
                        return dest.Fields[fldNumber].MinValue.ToString();
                    }

                case "СРЕДНЕЕ":
                    {
                        d = d + dest.Fields[fldNumber].AvgValue * perfCount;
                        dest.Fields[fldNumber].AvgValue = d / (perfCount + 1);
                        return dest.Fields[fldNumber].AvgValue.ToString();
                    }
            }
            return "";
        }
        #endregion
        
        #region Вывод
        public override string ToString()
        {
            return "[TABLE: " + Name + ";  Fields at all: " + Fields.Count + ";   Records at all: " + Records.Count +"]";
        }

        public void toPrintHeader()
        {
            string border = "".PadLeft(15, '=');
            Console.Write("=="); for (int i = 0; i < Fields.Count; i++) Console.Write("| " + border + " |"); Console.WriteLine();
            Console.Write("##"); foreach (FIELD field in Fields) Console.Write("| {0,-10}{1,5} |", field.Number+1, field.Type); Console.WriteLine();
            Console.Write("##"); foreach (FIELD field in Fields) Console.Write("| {0,-12}{1,3} |", field.Name.PadRight(12, ' ').Substring(0, 12), (field.IsPK ? "!" : " ")); Console.WriteLine();
            Console.Write("=="); for (int i = 0; i < Fields.Count; i++) Console.Write("| " + border + " |"); Console.WriteLine();
        }

        public void toPrint()
        {
            Console.WriteLine(this.ToString());
            toPrintHeader();

            // записи
            int number = 0;
            foreach (Dictionary<int, string> rec in Records)
            {
                if (rec == null) continue;

                Dictionary<int, string> temp = new Dictionary<int, string>(rec);

                Console.Write("{0,-2}", ++number);
                for (int i = 0; i < rec.Keys.Count; i++)
                {
                    foreach (int key in rec.Keys)
                    {
                        if (key == i)
                        {
                            Console.Write("| {0,15} |", rec[key]);
                        }
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("\r\n");
        }

        public void toPrintRecord(int number)
        {
            Dictionary<int, string> rec = Records.ElementAt(number);
            for (int i = 0; i < rec.Keys.Count; i++)                
            {
                Console.Write("| {0,-15} |", rec[i]);
            }
            Console.WriteLine();
        }
        #endregion
    }
}
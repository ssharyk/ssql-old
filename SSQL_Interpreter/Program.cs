﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SSQL_Scaner;
using System.IO;

namespace SSQL_Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SSQL_Scaner.Program.Main(null);
            }
            catch (Exception)
            {
                return;
            }


            #region Исполнение
            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("EXECUTING ");
            ////Console.WriteLine("================\n");

            INTERPRETER interpreter = new INTERPRETER();
            try
            {
                Console.WriteLine("\n================");
                Console.WriteLine("ВЫВОД, СГЕНЕРИРОВАННЫЙ СКРИПТОМ");
                Console.WriteLine("================");
                interpreter.toExecuteProgram();
                Console.WriteLine("================\n\n");

                Console.WriteLine("Вывести БД?  [Y/-Y]: ");
                Console.Write("--> ");
                string ans = Console.ReadLine();
                if (ans.ToUpper() == "Y" || ans.Length==0)
                    interpreter.toPrintDataBase();
            }
            catch (TranslatorException exc)
            {
                Console.WriteLine("\tRunTime ERROR");
                Console.WriteLine(exc.Message);
                //Console.WriteLine(exc.StackTrace);
                Console.ReadKey(); 
                return;
            }

            ////Console.WriteLine("\n\n================");
            ////Console.WriteLine("EXECUTING HAS FINISHED!  Press any key... ");
            Console.WriteLine("\nВыполнение программы завершено успешно...");
            ////Console.WriteLine("================\n\n");
            #endregion
            Console.ReadKey();
        }
    }
}

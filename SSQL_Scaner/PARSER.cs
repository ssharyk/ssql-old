﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SSQL_Scaner
{
    public class PARSER
    {
        #region Поля и свойства
        protected SCANER _scaner;
        protected static Dictionary<string, int> _priority;
        protected Stack<string> _stack;
        public List<string> PostfixForm { get; protected set; }
        protected int _currentToken;
        protected int _currentLabel;
        protected Dictionary<int, int> _labels;
        protected static string[] _dataTypeWords = { "ДИН", "ЦЕЛ", "ВЕЩ", "ЛОГ", "СТР" };
        protected int _aggrIndex;
        #endregion

        #region Основные методы
        public PARSER(SCANER scan)
        {
            _scaner = new SCANER(scan);
        }

        public void toMakeSyntaxAnalysis()
        {
            toInitPriorities();

            _stack = new Stack<string>();
            PostfixForm = new List<string>();
            _labels = new Dictionary<int, int>();

            LEXEME token;
            for (_currentToken = 0; _currentToken < _scaner.Tokens.Count; _currentToken++)
            {
                token = _scaner.toGetToken(_currentToken);
                toMakeDecision(token);                
            }

            PostfixForm.Add("$EOP");
        }

        protected void toMakeDecision(LEXEME token)
        {
            if (_dataTypeWords.Contains(token.Value)) PostfixForm.Add(token.Value);
            if (token.Type == LexemeType.ИмяКонстанты || token.Type == LexemeType.ИмяПеременной ||
                token.Type.ToString().StartsWith("Константа"))
            {
                PostfixForm.Add(token.Value);
            }

            if (token.Type == LexemeType.Присваивание) 
            {
                LEXEME prev = _scaner.toGetToken(_currentToken - 1);
                if (prev.Type == LexemeType.ИмяКонстанты || prev.Type == LexemeType.ИмяПеременной ||
                    prev.Type==LexemeType.ИмяПоля)
                {
                    _stack.Push(":");
                    toParseExpression(true);
                }
                else
                {
                    toHandleError("Присваивание должно применяться к переменной/константе");
                    return;
                }
            }

            if (token.Type == LexemeType.КлючевоеСлово)
            {
                switch (token.Value)
                {
                    case "ВЫВОД":
                        {
                            toParsePRINT();
                            break;
                        }
                    case "ЕСЛИ":
                        {
                            toParseIF();
                            break;
                        }
                    case "СОЗДАТЬ":
                        {
                            toParseCREATE();
                            break;
                        }
                    case "УДАЛИТЬ":
                        {
                            toParseDELETE_TABLE();
                            break;
                        }

                    case "В":
                        {
                            LEXEME lex = _scaner.toGetToken(++_currentToken);
                            if (lex.Type != LexemeType.ИмяТаблицы)
                            {
                                toHandleRequirement("Имя таблицы", lex);
                                return;
                            }
                            PostfixForm.Add(lex.Value);

                            lex = _scaner.toGetToken(++_currentToken);
                            if (lex.Value == "ВСТАВИТЬ") { toParseINSERT(); PostfixForm.Add("ВСТАВИТЬ"); break; }
                            if (lex.Value == "МОДИФ") { toParseMODIFY(); PostfixForm.Add("МОДИФ"); break; }

                            toHandleRequirement("операция", lex);
                            break;
                        }

                    case "ИЗ":
                        {
                            LEXEME lex = _scaner.toGetToken(++_currentToken);
                            if (lex.Type != LexemeType.ИмяТаблицы)
                            {
                                toHandleRequirement("Имя таблицы", lex);
                                return;
                            }

                            lex = _scaner.toGetToken(++_currentToken);
                            if (lex.Value == "УДАЛИТЬ") { PostfixForm.Add(_scaner.toGetToken(_currentToken - 1).Value);  toParseDELETE(); PostfixForm.Add("УДАЛИТЬ"); break; }
                            if (lex.Value == "ВЫБРАТЬ" || lex.Value==",") { _currentToken-=3; toParseSELECT(); break; }

                            toHandleRequirement("операция", lex);
                            break;
                        }

                    case "ДЛЯ":
                        {
                            toParseFOREACH();
                            break;
                        }
                }
            }
        }
        
        #endregion
        
        #region Control types
        
        public void toControlTypes()
        {
        	List<string> rpn = new List<string>(PostfixForm);
        	for (int i = 0; i < rpn.Count; i++)
        	{
        		//string s = tokens[i].Value;
        		string s = rpn[i];
        		if (s[0] == '+' || s[0] == '-' || s[0] == '*' || s[0] == '/' || s[0]=='%' || s[0]=='^' ||
                    s[0] == '>' || s[0] == '<' || s[0] == '=' || s[0] == '~' ||
                    s == "ИЛИ" || s == "И" || s == "НЕ" )
        		{
        			rpn = toControlTypes(rpn, i);
        		}
        	}
        }
        
        private List<string> toControlTypes(List<string> rpn, int index)
        {
        	string command = rpn[index];
        	
        	if (isAssignmentOperator(command))
        	{
        		var op1 = rpn[index-2]; DataType dt1 = _scaner.toGetType(op1);
        		var varName = rpn[index - 1]; DataType dt2 = _scaner.toGetType(varName);
        		if (dt1 != dt2)
    				throw new TranslatorException($"Cannot assign '{varName}' value '{op1}' as different types: {dt1} and {dt2}");
        		
        		// exclude assignment at all
        		rpn = toReplaceWithFake(rpn, index, 3, "");
        		rpn.RemoveAt(index);
        		return rpn;
        	}
    		
        	if (isComparisonOperator(command))
    		{
    			var op1 = rpn[index-2]; DataType dt1 = _scaner.toGetType(op1);
    			var op2 = rpn[index-1]; DataType dt2 = _scaner.toGetType(op2);
    			if (dt1 != dt2)
    				throw new TranslatorException($"Cannot compare '{command}' different types: '{op1}' of {dt1} and '{op2}' of {dt2}");
    			
    			// otherwise, replace with single (fake) value
    			return toReplaceWithFake(rpn, index, 3, "ЛОЖЬ");
    		}
    		
    		if (isBooleanOperator(command))
    		{
    			if (isUnarOperator(command))
    			{
    				string oper = rpn[index-1];
	    			DataType dt = _scaner.toGetType(oper);
	        		if (dt != DataType.ЛОГ)
	        			throw new TranslatorException($"Для '{command}'('{oper}') требуется логический операнд");
	        		return toReplaceWithFake(rpn, index, 3, "ИСТИНА");
    			}
    		}
    		
    		if (isStringOperator(command))
    		{
    			var op1 = rpn[index-2]; DataType dt1 = _scaner.toGetType(op1);
    			if (dt1 == DataType.СТР)
    			{
    				var op2 = rpn[index-1]; DataType dt2 = _scaner.toGetType(op2);
    				if (dt2 == DataType.СТР)
    				{
    					if (command != "+")
    						throw new TranslatorException("For strings only concatenation allowed");
    					return toReplaceWithFake(rpn, index, 3, "\"ConcatenatedString\"");
    				}
    				if (dt2 == DataType.ВЕЩ)
    				{
    					if (command != "*")
    						throw new TranslatorException("For string and number only repeating allowed");
    					return toReplaceWithFake(rpn, index, 3, "\"RepeatedString\"");
    				}
    			}
    			else
    			{
    				// may be, number? so, continue with number checking
    			}
    		}

    		if (isNumbersOperator(command))
    		{
    			var op1 = rpn[index-2]; DataType dt1 = _scaner.toGetType(op1);
    			var op2 = rpn[index-1]; DataType dt2 = _scaner.toGetType(op2);
    			if (dt1 != dt2 && dt1 != DataType.ДИН && dt2 != DataType.ДИН)
    				throw new TranslatorException($"Operator '{command}' is not allowed for types '{dt1}' and '{dt2}'");
    			if (dt1 != DataType.ВЕЩ && dt1 != DataType.ДИН)
    				throw new TranslatorException($"Expected ВЕЩ but found {dt1} for {op1}");
    			if (dt2 != DataType.ВЕЩ && dt2 != DataType.ДИН)
    				throw new TranslatorException($"Expected ВЕЩ but found {dt2} for {op2}");
    			return toReplaceWithFake(rpn, index, 3, "0");
    		}

    		return rpn;
        }
        
        private List<string> toReplaceWithFake(List<string> rpn, int index, int count, string newFakeValue)
        {
        	rpn.RemoveRange(index-count+1, count);
    		rpn.Insert(index-count+1, newFakeValue);
    		return rpn;
        }
        
        private bool isAssignmentOperator(string command)
        {
        	return command == ":";
        }

        private bool isComparisonOperator(string command)
        {
        	return command == "=" ||
        		command == ">=" ||
        		command == "<=" ||
        		command == ">" ||
        		command == "<" ||
        		command == "<>";
        }
        
        private bool isNumbersOperator(string command)
        {
        	return command == "+" ||
        		command == "-" ||
        		command == "*" ||
        		command == "/" ||
        		command == "%";
        }
        
        private bool isBooleanOperator(string command)
        {
        	return command == "НЕ" ||
        		command == "И" ||
        		command == "ИЛИ";
        }
        
        private bool isStringOperator(string command)
        {
        	return command == "+" ||
        		command == "*";
        }
        
        private bool isUnarOperator(string command)
        {
        	return command == "НЕ";
        }

        #endregion

        protected void toInitPriorities()
        {
            _priority = new Dictionary<string, int>();

            _priority.Add("(", 0);
            _priority.Add("[", 0);
            _priority.Add("ЕСЛИ", 0);
            _priority.Add("ДЛЯ", 0);

            _priority.Add(")", 1);
            _priority.Add("]", 1);
            _priority.Add(",", 1);
            _priority.Add(";", 1);
            _priority.Add(".", 1);
            _priority.Add("ТО", 1);
            _priority.Add("ИНАЧЕ", 1);

            _priority.Add(":", 2);

            _priority.Add("ИЛИ", 3);

            _priority.Add("И", 4);

            _priority.Add("==", 5);
            _priority.Add("<>", 5);

            _priority.Add(">", 6);
            _priority.Add(">=", 6);
            _priority.Add("<", 6);
            _priority.Add("<=", 6);

            _priority.Add("+", 7);
            _priority.Add("-", 7);

            _priority.Add("*", 8);
            _priority.Add("/", 8);
            _priority.Add("%", 8);

            _priority.Add("^", 9);
            _priority.Add("~", 9);
            _priority.Add("НЕ", 9);
        }

        #region Преобразование в ОПЗ
        #region Команды
        protected void toParseExpression(bool forEnd)
        {
            string s="";
            do
            {
                LEXEME lex = _scaner.toGetToken(++_currentToken);
                s = lex.Value;

                if (lex.Type.ToString().StartsWith("Имя") || lex.Type.ToString().StartsWith("Константа"))
                {
                    PostfixForm.Add(s);
                    //continue;
                }
                if (
                    (s[0] == '+' || s[0] == '-' || s[0] == '*' || s[0] == '/' || s[0]=='%' || s[0]=='^' ||
                    s[0] == '>' || s[0] == '<' || s[0] == '=' || s[0] == '~' ||
                    s == "ИЛИ" || s == "И" || s == "НЕ" ))
                {
                    while (_stack.Count > 0 && _priority[_stack.Peek()] >= _priority[s])
                    {
                        PostfixForm.Add(_stack.Pop());
                    }

                    if (_stack.Count == 0 || _priority[_stack.Peek()] < _priority[s])
                    {
                        _stack.Push(s);
                    }
                }

                if (s[0] == '(')
                {
                    _stack.Push(s);
                    //toParseASSIGNMENT();
                }

                if (s[0] == ')')
                {
                    string s1 = "";
                    do
                    {
                        s1 = _stack.Pop();
                        if (s1 != "(") PostfixForm.Add(s1);
                    } while (s1 != "(");
                }
            } while (s!=";" && s!="ТО" && s!="ИНАЧЕ" && s!="." && s!="," && s!="ГДЕ");
                       
            if (forEnd)
            {
                string s2="";
                while (_stack.Count > 0)
                {
                    s2 = _stack.Pop();
                    if (s2!="(") PostfixForm.Add(s2);
                }
                toControlTypes();
            }
        }

        protected void toParsePRINT()
        {
            int numberOfParameters = 0;
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }

            LEXEME val;
            do
            {
                val = _scaner.toGetToken(++_currentToken);
                tok = _scaner.toGetToken(++_currentToken);
                if (val.Value == ")")
                {
                    _currentToken--;
                    break;
                }

                if (tok.Value != "," && tok.Value != ")")
                {
                    toHandleRequirement(",", tok);
                    return;
                }
                if (val.Type != LexemeType.ИмяКонстанты &&
                    val.Type != LexemeType.ИмяПеременной &&
                    !val.Type.ToString().StartsWith("Константа"))
                {
                    toHandleRequirement("переменная/константа", val);
                    return;
                }
                
                numberOfParameters++;
                toMakeDecision(val);
            } while (tok.Value != ")");

            val = _scaner.toGetToken(++_currentToken);
            if (val.Value != ".")
            {
                toHandleRequirement(".", val);
                return;
            }

            PostfixForm.Add(numberOfParameters.ToString());
            PostfixForm.Add("ВЫВОД");
        }

        protected void toParseIF()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }
            _stack.Push("(");

            toParseExpression(true);
            PostfixForm.Add("@" + ++_currentLabel);
            PostfixForm.Add("УПЛ");
            int lbl = _currentLabel;

            tok=_scaner.toGetToken(_currentToken);
            if (tok.Value!="ТО")
            {
                toHandleRequirement("ТО", tok);
                return;
            }

            do
            {
                tok = _scaner.toGetToken(++_currentToken);
                toMakeDecision(tok);

            } while (tok.Value != "." && tok.Value != "ИНАЧЕ");
            _labels[_currentLabel] = PostfixForm.Count;     // метка по УПЛ

            
            tok = _scaner.toGetToken(_currentToken);
                        
            if (tok.Value == "ИНАЧЕ")
            {
                PostfixForm.Add("@" + ++_currentLabel);
                PostfixForm.Add("БП");
                _labels[lbl] = PostfixForm.Count;

                int lblElse = _currentLabel;
                do
                {
                    tok = _scaner.toGetToken(++_currentToken);
                    toMakeDecision(tok);
                } while (tok.Value != ".");

                _labels[lblElse] = PostfixForm.Count;
            }

            if (tok.Value != ".")
            {
                toHandleRequirement(".", tok);
                return;
            }
        }

        protected void toParseFOREACH()
        {
            //Console.WriteLine("\n\nCYCLE For-Each:\n==================");

            LEXEME tok = _scaner.toGetToken(++_currentToken);
            //Console.WriteLine("First token is " + tok);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }

            int lbl1 = _currentLabel + 1;       // первая метка запроса
            toParseSELECT();
            // теперь _curLabel : lbl1 + 2;

            
            int start = _labels[_currentLabel - 1];
            //Console.WriteLine("start = " + start);
            List<string> rest = PostfixForm.GetRange(start, PostfixForm.Count-start);
            PostfixForm.RemoveRange(start, PostfixForm.Count - start);


            //////Console.WriteLine("REST:");
            //////foreach (string s in rest)
            //////    Console.WriteLine("\t" + s);
            //////Console.WriteLine("# REST");
            //////toPrintPostfixForm();


            // тело цикла
            int parameters = 0;
            do
            {
                tok = _scaner.toGetToken(++_currentToken);
                //Console.WriteLine("\t   work with " + tok);

                if (tok.Type == LexemeType.КлючевоеСлово || tok.Type == LexemeType.Присваивание)
                    parameters++;

                toMakeDecision(tok);
            } while (tok.Type != LexemeType.КонецВыражения);



            _labels[lbl1+1] = PostfixForm.Count;
            PostfixForm.AddRange(rest);

            _labels[lbl1] = PostfixForm.Count;
            
            //PostfixForm.Add(parameters.ToString());
            
            //Console.WriteLine("\n==================\nCYCLE For-Each #\n\n");
        }
        #endregion

        #region Запросы-создание
        protected void toParseCREATE()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            switch (tok.Value)
            {
                case "ТАБЛИЦУ":
                    {
                        toParseCREATE_TABLE();
                        break;
                    }

                case "КЛЮЧ":
                    {
                        toParseCREATE_KEY();
                        break;
                    }

                case "СВЯЗЬ":
                    {
                        toParseCREATE_LINK();
                        break;
                    }

                default:
                    {
                        toHandleRequirement("ТАБЛИЦУ, КЛЮЧ или СВЯЗЬ", tok);
                        return;
                    }
            }
        }

        protected void toParseCREATE_TABLE()
        {
            // имя
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Type != LexemeType.ИмяТаблицы)
            {
                toHandleRequirement("имя таблицы", tok);
                return;
            }
            PostfixForm.Add(tok.Value);

            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }

            int parameters = 1;
            LEXEME val, delim;
            do
            {
                tok = _scaner.toGetToken(++_currentToken);
                val = _scaner.toGetToken(++_currentToken);
                delim = _scaner.toGetToken(++_currentToken);

                if (!_dataTypeWords.Contains(tok.Value))
                {
                    toHandleRequirement("Тип данных", tok);
                    return;
                }

                if (val.Type != LexemeType.ИмяПоля)
                {
                    toHandleRequirement("Имя поля таблицы", val);
                    return;
                }

                if (delim.Value != "," && delim.Value != ")")
                {
                    toHandleRequirement(", или )", delim);
                    return;
                }

                PostfixForm.Add(tok.Value);
                PostfixForm.Add(val.Value);
                parameters += 2;
            } while (delim.Value != ")");

            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != ".")
            {
                toHandleRequirement(".", tok);
                return;
            }

            PostfixForm.Add(parameters.ToString());
            PostfixForm.Add("СОЗДАТЬ_Т");
        }

        protected void toParseCREATE_KEY()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Type != LexemeType.ИмяТаблицы)
            {
                toHandleRequirement("имя таблицы", tok);
                return;
            }
            string tblName = tok.Value;

            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }

            int parameters = 0;
            do
            {
                tok = _scaner.toGetToken(++_currentToken);
                if (tok.Type != LexemeType.ИмяПоля)
                {
                    toHandleRequirement("имя поля", tok);
                    return;
                }
                parameters++;
                PostfixForm.Add(tok.Value);

                tok = _scaner.toGetToken(++_currentToken);
            } while (tok.Value == ",");

            tok = _scaner.toGetToken(_currentToken);
            if (tok.Value != ")")
            {
                toHandleRequirement(")", tok);
                return;
            }

            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != ".")
            {
                toHandleRequirement(".", tok);
                return;
            }

            PostfixForm.Add(parameters.ToString());
            PostfixForm.Add(tblName);
            PostfixForm.Add("СОЗДАТЬ_К");
        }

        protected void toParseCREATE_LINK()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }

            toParseOneLink();
            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != ",")
            {
                toHandleRequirement(",", tok);
                return;
            }
            toParseOneLink();
            PostfixForm.Add("СОЗДАТЬ_С");
        }

        protected void toParseOneLink()
        {
            LEXEME t_nm = _scaner.toGetToken(++_currentToken);
            if (t_nm.Type != LexemeType.ИмяТаблицы)
            {
                toHandleRequirement("Имя таблицы", t_nm);
                return;
            }

            LEXEME delim = _scaner.toGetToken(++_currentToken);
            if (delim.Value!="!")
            {
                toHandleRequirement("!", t_nm);
                return;
            }

            LEXEME f_nm = _scaner.toGetToken(++_currentToken);
            if (f_nm.Type != LexemeType.ИмяПоля)
            {
                toHandleRequirement("Имя поля", t_nm);
                return;
            }

            PostfixForm.Add(t_nm.Value);
            PostfixForm.Add(f_nm.Value);
        }

        protected void toParseDELETE_TABLE()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "ТАБЛИЦУ")
                toHandleRequirement("ключевое слово: таблицу/связь/...", tok);
            
            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Type != LexemeType.ИмяТаблицы)
                toHandleRequirement("имя таблицы", tok);
            
            LEXEME e = _scaner.toGetToken(++_currentToken);
            if (e.Value!=".")
                toHandleRequirement(".", e);

            PostfixForm.Add(tok.Value);
            PostfixForm.Add("УДАЛИТЬ_Т");
        }
        #endregion

        #region Запросы-модификация
        protected void toParseINSERT()
        {
            int parameters = 1;
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }
            _stack.Push("(");

            LEXEME val;
            do
            {
                val = _scaner.toGetToken(++_currentToken);
                PostfixForm.Add(val.Value);
                toParseExpression(false);

                tok = _scaner.toGetToken(_currentToken);
                if (tok.Value != "," && tok.Value != ".")
                {
                    toHandleRequirement(", или )", tok);
                    return;
                }

                parameters++;

                while (_stack.Count > 1)
                {
                    PostfixForm.Add(_stack.Pop());
                }
            } while (tok.Value != ".");
            
            tok = _scaner.toGetToken(_currentToken);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                toHandleRequirement(".", tok);
                return;
            }

            PostfixForm.Add(parameters.ToString());
        }

        protected void toParseMODIFY()
        {
            int parameters = 1;
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }
            _stack.Push("(");
           
            string tblName=_scaner.toGetToken(_currentToken - 2).Value;
            PostfixForm.Add("ВЕЩ");
            string iter=tblName + "_ITERATOR_" + 2;
            _scaner.toAddIterator(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("0");
            PostfixForm.Add(":");


            int lbl3Hop=PostfixForm.Count;
            PostfixForm.Add(iter);
            PostfixForm.Add("LENGTH");
            PostfixForm.Add("<");
            
            
            int lbl1=++_currentLabel;
            PostfixForm.Add("@" + lbl1);
            PostfixForm.Add("УПЛ");
                        

            int start = PostfixForm.Count;
            LEXEME nm;
            do
            {
                nm = _scaner.toGetToken(++_currentToken);
                tok = _scaner.toGetToken(++_currentToken);

                if (tok.Type != LexemeType.Присваивание)
                {
                    toHandleRequirement(":", tok);
                    return;
                }

                parameters++;
                PostfixForm.Add(nm.Value);
                toParseExpression(true);
                _stack.Push("(");
                PostfixForm.Add(":");

                tok = _scaner.toGetToken(_currentToken);
            } while (tok.Type != LexemeType.КонецВыражения && tok.Value!="ГДЕ");

            int finish = PostfixForm.Count;

            List<string> body = PostfixForm.GetRange(start, finish - start);
            PostfixForm.RemoveRange(start, finish - start);

            tok = _scaner.toGetToken(_currentToken);
            bool isCond = false;
            if (tok.Value == "ГДЕ")
            {
                isCond = true;
                toParseExpression(true);
            }

            tok = _scaner.toGetToken(_currentToken);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                toHandleRequirement(".", tok);
                return;
            }

            int lbl2 = ++_currentLabel;
            if (isCond)
            {
                PostfixForm.Add("@" + lbl2);
                PostfixForm.Add("УПЛ");
            }

            PostfixForm.AddRange(body);

            int lbl2Hop=PostfixForm.Count;

            PostfixForm.Add(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("1");
            PostfixForm.Add("+");
            PostfixForm.Add(":");

            int lbl3 = ++_currentLabel;
            PostfixForm.Add("@" + _currentLabel++);
            PostfixForm.Add("БП");

            _labels[lbl1] = PostfixForm.Count+1;
            _labels[lbl2] = lbl2Hop;
            _labels[lbl3] = lbl3Hop;
        }

        protected void toParseDELETE()
        {
            PostfixForm.Add("ВЕЩ");
            string tblName = _scaner.toGetToken(_currentToken - 1).Value;
            string iter = tblName + "_ITERATOR_" + 3;
            _scaner.toAddIterator(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("0");
            PostfixForm.Add(":");


            int lbl3Hop = PostfixForm.Count;

            PostfixForm.Add(iter);
            PostfixForm.Add("LENGTH");
            PostfixForm.Add("<=");

            int lbl1 = ++_currentLabel;
            PostfixForm.Add("@" + lbl1);
            PostfixForm.Add("УПЛ");




            LEXEME tok = _scaner.toGetToken(++_currentToken);
            //Console.WriteLine("DELETE: " + tok);
            int start = PostfixForm.Count;

            if (tok.Value == "ГДЕ")
            {
                toParseExpression(true);                
            }

            PostfixForm.Add("@" + ++_currentLabel);
            PostfixForm.Add("УПЛ");
            //                _labels[_currentLabel] = start;

            int lbl2 = _currentLabel;


            tok = _scaner.toGetToken(_currentToken);
            //Console.WriteLine("DELETE: " + tok);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                toHandleRequirement(".", tok);
                return;
            }

            //PostfixForm.Add(tblName);
            //PostfixForm.Add(iter);
            //PostfixForm.Add("]");

            //PostfixForm.Add("null");
            //PostfixForm.Add(":");
            int lbl2Hop = PostfixForm.Count;

            PostfixForm.Add(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("1");
            PostfixForm.Add("+");
            PostfixForm.Add(":");

            int lbl3 = ++_currentLabel;
            PostfixForm.Add("@" + lbl3);
            PostfixForm.Add("БП");



            _labels[lbl1] = PostfixForm.Count+1;
            _labels[lbl2] = lbl2Hop;
            _labels[lbl3] = lbl3Hop;
        }
        #endregion

        #region Выборка
        protected void toParseSELECT()
        {
            LEXEME tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "ИЗ")
            {
                toHandleRequirement("ИЗ", tok);
                return;
            }

            bool isManyTables=false;
            string tblName="undefined";
            do
            {
                tok = _scaner.toGetToken(++_currentToken);
                if (tok.Type != LexemeType.ИмяТаблицы)
                {
                    toHandleRequirement("имя таблицы", tok);
                    return;
                }
                PostfixForm.Add(tok.Value);

                if (tblName != "undefined")
                    isManyTables = true;

                tblName = tok.Value;
                tok = _scaner.toGetToken(++_currentToken);
                if (tok.Value != "," && tok.Value != "ВЫБРАТЬ")
                    toHandleRequirement(", или ВЫБРАТЬ", tok);

            } while (tok.Value != "ВЫБРАТЬ");
           
            if (tok.Value!="ВЫБРАТЬ")
            {
                toHandleRequirement("команда (выбор)", tok);
                return;
            }

            tok = _scaner.toGetToken(++_currentToken);
            if (tok.Value != "(")
            {
                toHandleRequirement("(", tok);
                return;
            }


            PostfixForm.Add("ВЕЩ");
            string iter = tblName + "_ITERATOR_" + 1;
            _scaner.toAddIterator(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("0");
            PostfixForm.Add(":");




            _aggrIndex = PostfixForm.Count;
            int agInd = _aggrIndex;




            int lbl3Hop = PostfixForm.Count;
            PostfixForm.Add(iter);
            PostfixForm.Add("LENGTH");
            PostfixForm.Add("<");


            int lbl1 = ++_currentLabel;
            PostfixForm.Add("@" + lbl1);
            PostfixForm.Add("УПЛ");






            int bodyStart = PostfixForm.Count;

            // выбираемые поля
            toParseSelectedFields(isManyTables);
            int bodyFinish = PostfixForm.Count;
            bodyStart += (_aggrIndex - agInd);

            List<string> body = PostfixForm.GetRange(bodyStart, bodyFinish - bodyStart);
            PostfixForm.RemoveRange(bodyStart, bodyFinish - bodyStart);
            

            tok = _scaner.toGetToken(++_currentToken);
            int lbl2 = ++_currentLabel;
            if (tok.Value == "ГДЕ")
            {
                int start = PostfixForm.Count;
                toParseExpression(true);                
            }
            PostfixForm.Add("@" + lbl2);
            PostfixForm.Add("УПЛ");

            PostfixForm.AddRange(body);


            int lbl2Hop = PostfixForm.Count;
            PostfixForm.Add(iter);
            PostfixForm.Add(iter);
            PostfixForm.Add("1");
            PostfixForm.Add("+");
            PostfixForm.Add(":");

            PostfixForm.Add("ВЫБРАТЬ");
            int lbl3 = ++_currentLabel;
            PostfixForm.Add("@" + lbl3);
            PostfixForm.Add("БП");


            _labels[lbl1] = PostfixForm.Count ;
            _labels[lbl2] = lbl2Hop;
            _labels[lbl3] = lbl3Hop + (_aggrIndex-agInd);



            tok = _scaner.toGetToken(_currentToken);
            if (tok.Type!=LexemeType.КонецВыражения)
            {
                toHandleRequirement(".", tok);
                return;
            }
        }

        protected void toParseSelectedFields(bool isManyTables)
        {
            LEXEME nm, var, tok;
            string tblName = (isManyTables) ? "undefined" : PostfixForm[PostfixForm.Count - 10];

            do
            {
                // описание переменной
                var = _scaner.toGetToken(++_currentToken);
                if (_dataTypeWords.Contains(var.Value))
                {
                    PostfixForm.Add(var.Value);
                    var = _scaner.toGetToken(++_currentToken);
                }
                else
                {
                    throw new TranslatorException("Ожидается тип переменной");
                }

                if (var.Type != LexemeType.ИмяПеременной)
                {
                    toHandleRequirement("переменная ", var);
                    return;
                }
                PostfixForm.Add(var.Value);

                // присваивание
                tok = _scaner.toGetToken(++_currentToken);
                if (tok.Type != LexemeType.Присваивание)
                {
                    toHandleRequirement(":", tok);
                    return;
                }

                // имя поля
                nm = _scaner.toGetToken(++_currentToken);
                if (nm.Value == "НАИБ" || nm.Value == "НАИМ" || nm.Value == "СРЕДНЕЕ")
                {
                    tok = _scaner.toGetToken(++_currentToken);
                    if (tok.Value != "(")
                    {
                        toHandleRequirement("(", tok);
                        return;
                    }

                    if (isManyTables)
                    {
                        LEXEME tb = _scaner.toGetToken(++_currentToken);
                        tblName = tb.Value;
                        tok = _scaner.toGetToken(++_currentToken);
                        if (tok.Value != "!")
                            toHandleRequirement("!", tok);
                        //nm = _scaner.toGetToken(++_currentToken);
                    }
                    PostfixForm.Add(tblName);

                    nm = _scaner.toGetToken(++_currentToken);
                    if (nm.Type != LexemeType.ИмяПоля)
                    {
                        toHandleRequirement("имя поля", nm);
                        return;
                    }

                    tok = _scaner.toGetToken(++_currentToken);
                    if (tok.Value != ")")
                    {
                        toHandleRequirement(")", tok);
                        return;
                    }

                    PostfixForm.Add(nm.Value);
                    //PostfixForm.Add("1");
                    LEXEME check = (isManyTables) ? _scaner.toGetToken(_currentToken - 5) : _scaner.toGetToken(_currentToken - 3);
                    PostfixForm.Add(check.Value);
                    
                    PostfixForm.Add(":");
                    
                    
                    ////////////////int now = PostfixForm.Count;
                    ////////////////List<string> aggr = PostfixForm.GetRange(now - 5, 5);
                    ////////////////PostfixForm.RemoveRange(now - 5, 5);
                    ////////////////PostfixForm.InsertRange(_aggrIndex, aggr);                    
                    ////////////////_aggrIndex += 5;

                    tok = _scaner.toGetToken(++_currentToken);
                    continue;
                }
                else
                {
                    if (isManyTables)                      
                    {
                        tblName = nm.Value;
                        tok = _scaner.toGetToken(++_currentToken);
                        if (tok.Value != "!")
                            toHandleRequirement("!", tok);
                        nm = _scaner.toGetToken(++_currentToken);
                    }
                    PostfixForm.Add(tblName);

                    if (nm.Type != LexemeType.ИмяПоля)
                    {
                        toHandleRequirement("имя поля", nm);
                        return;
                    }
                    PostfixForm.Add(nm.Value);
                }

                PostfixForm.Add(":");

                tok = _scaner.toGetToken(++_currentToken);
            } while (tok.Value != ")");
        }
        #endregion
        #endregion

        #region Вывод
        public void toPrintPostfixForm()
        {
            Console.WriteLine("\n\nPOSTFIX FORM:");
            for (int i = 0; i < PostfixForm.Count; i++)
            {
                Console.WriteLine("#{0}  -->   {1}", i, PostfixForm.ElementAt(i));
            }
            Console.WriteLine("\n..............\n");
        }

        protected void toPrintPostfixForm(int start)
        {
            Console.WriteLine("\n\nPOSTFIX FORM: from" + start);
            for (int i = start; i < PostfixForm.Count; i++)
            {
                Console.WriteLine("#{0}  -->   {1}", i, PostfixForm.ElementAt(i));
            }
            Console.WriteLine("\n..............\n");
        }

        protected void toPrintStack()
        {
            Stack<string> st = new Stack<string>(_stack);
            Console.WriteLine("\t\t IN STACK:");
            while (st.Count > 0)
            {
                Console.Write(st.Pop());
            }
            Console.WriteLine();
            //Console.WriteLine("      " + PostfixForm);
        }

        public void toPrintLabels()
        {
            Console.WriteLine("\n\nLABELS:");
            foreach (int i in _labels.Keys)
            {
                Console.WriteLine("HOP {0} to {1}", i, _labels[i]);
            }
            Console.WriteLine("..........");
        }
        #endregion

        #region Исключения
        protected void toHandleRequirement(string req, LEXEME op)
        {
            throw new TranslatorException("REQUIRE:  " + req + "\nВ строке " + op.Line + " символ " + op.Symbol);
        }

        protected void toHandleError(string mes)
        {
            throw new TranslatorException(mes);
        }
        #endregion

        // Запись ОПЗ в файл
        public void toWriteIntoFile()
        {
            FileStream fs = new FileStream("..\\..\\..\\program.rpn", FileMode.Create);
            StreamWriter file = new StreamWriter(fs, Encoding.UTF8);

            file.WriteLine(PostfixForm.Count);
            foreach (string s in PostfixForm)
            {
                file.WriteLine(s);
            }

            //int c =0;
            //foreach (IDENTIFIER id in _scaner.Identifiers)
            //    if (id.Name[0] == '_' || id.Name.IndexOf("_ITERATOR_")!=-1) c++;
            //file.WriteLine(c);
            //foreach(IDENTIFIER id in _scaner.Identifiers)
            //{
            //    if (id.Name[0] == '_' || id.Name.IndexOf("_ITERATOR_") != -1)
            //    {
            //        file.WriteLine(id.ToString());
            //    }
            //}

            file.WriteLine(_labels.Count);
            foreach (int i in _labels.Keys)
            {
                file.WriteLine(i + " " + _labels[i]);
            }

            file.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSQL_Interpreter
{
    public class QueryParameter
    {
        public TABLE Table { get; set; }
        public string VariableName { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public bool IsAggregate { get; set; }

        public QueryParameter(TABLE table, string v_nm, string f_nm, string v, bool aggr)
        {
            Table = table;
            VariableName = v_nm;
            FieldName = f_nm;
            FieldValue = v;
            IsAggregate = aggr;
        }
    }

    public class QueryInfo
    {
        public int Iterator { get; set; }
        public int IsInQuery { get; set; }
        public TABLE CurrentTable { get; set; }
        public int NumberOfPerformedRecords { get; set; }
        public bool IsNeedPrintRecord { get; set; }
        public bool IsQueryFinish { get; set; }
        public int QueryBody { get; set; }
        public List<QueryParameter> QueryParameters { get; set; }
        public TABLE ResultTable { get; set; }

        public QueryParameter toGetParameterByVariableName(string varName)
        {
            foreach (QueryParameter par in QueryParameters)
            {
                if (par.VariableName == varName)
                    return par;
            }
            return null;
        }
    }
}
